<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/testing', 'TagController@test');

Route::get('/', 'PagesController@browse')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'gallery'], function(){
	Route::get('/{slug}', 'GalleryController@view')->name('gallery.view');

	Route::get('/{id}/r/{page?}', 'GalleryController@read')->name('gallery.read');

});

Route::group(['prefix' => 'admin', 'middleware' => 'role:admin'], function(){
	Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');
	Route::group(['prefix' => 'users'], function(){
		Route::get('/', 'AdminController@users')->name('admin.user');

		Route::get('/update/{id}', 'AdminController@userUpdate')->name('admin.user.update');
		Route::post('/update/{id}', 'AdminController@userUpdatePost')->name('admin.user.update');
		Route::group(['prefix' => 'roles'], function(){
			Route::get('/', 'AdminController@roles')->name('admin.user.roles');
			Route::get('/update/{id}', 'AdminController@roleUpdate')->name('admin.user.roles.update');
			Route::post('/update/{id}', 'AdminController@roleUpdatePost')->name('admin.user.roles.update');

			Route::get('/create', 'AdminController@roleCreate')->name('admin.user.roles.create');
			Route::post('/create', 'AdminController@roleCreatePost')->name('admin.user.roles.create');
		});
		Route::group(['prefix' => 'permissions'], function(){
			Route::get('/', 'AdminController@permissions')->name('admin.user.permissions');
			Route::get('/update/{id}', 'AdminController@permissionUpdate')->name('admin.user.permissions.update');
			Route::post('/update/{id}', 'AdminController@permissionUpdatePost')->name('admin.user.permissions.update');
		});
	});

	Route::group(['prefix' => 'gallery'], function(){
		Route::get('/', 'AdminController@galleries')->name('admin.gallery');
		Route::get('/update/{id}', 'AdminController@updateGallery')->name('admin.gallery.update');
		Route::post('/update', 'AdminController@updateGalleryPost')->name('admin.gallery.update.post');

		Route::get('/delete/{id}', 'AdminController@deleteGallery')->name('admin.gallery.delete');
		Route::get('/recover/{id}', 'AdminController@recoverGallery')->name('admin.gallery.recover');
	});

	Route::group(['prefix' => 'tag'], function(){
		Route::get('/', 'AdminController@tags')->name('admin.tag');
		Route::get('/update/{id}', 'AdminController@updateTag')->name('admin.tag.update');
		Route::post('/update/{id}', 'AdminController@updateTagPost')->name('admin.tag.update');

		Route::get('/delete/{id}', 'AdminController@deleteTag')->name('admin.tag.delete');
		Route::get('/recover/{id}', 'AdminController@recoverTag')->name('admin.tag.recover');
	});
});

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function(){
	Route::group(['prefix' => 'setting'], function(){
		Route::view('/', 'user.setting.index')->name('user.setting');

		Route::view('/password', 'user.setting.password')->name('user.setting.password');
		Route::post('/password', 'UserController@changePassword')->name('user.setting.password');

		Route::view('/email', 'user.setting.email')->name('user.setting.email');
		Route::post('/email', 'UserController@changeEmail')->name('user.setting.email');

		Route::view('/name', 'user.setting.name')->name('user.setting.name');
		Route::post('/name', 'UserController@setName')->name('user.setting.name');
	});

	Route::group(['prefix' => 'profile'], function(){
		Route::get('/{slug}', 'UserController@authUserProfile')->name('user.profile');
		Route::group(['prefix' => 'me'], function(){
		});
	});
	Route::get('/me', 'UserController@authUserProfile')->name('user.auth.profile');
	Route::get('/uploads', 'UserController@authUploads')->name('user.auth.uploads');
	Route::get('/favorites', 'UserController@authFavorites')->name('user.auth.favorites');
	Route::get('/bookmarks', 'UserController@authBookmark')->name('user.auth.bookmarks');
});

Route::group(['prefix' => 'tags'], function(){
	Route::get('/', 'TagController@index')->name('tag.index');
	Route::get('{type}/{sort?}', 'TagController@sort')->name('tag.sort');
	Route::get('{type}/v/{slug}/{sort?}', 'TagController@view')->name('tag.view');

	Route::post('/search/{type}/', 'TagController@search');
});

Route::group(['prefix' => 'contributions'], function(){
	Route::get('/upload', 'ContributionController@upload')->name('contribute.upload.index');
	Route::post('/upload', 'ContributionController@uploadPost')->name('contribute.upload.post');

	Route::get('/tag', 'ContributionController@tag')->name('contribute.tag.index');
	Route::post('/tag', 'ContributionController@tagPost')->name('contribute.tag.post');

	Route::get('/', 'ContributionController@userContributions')->name('contribute');
	Route::get('/gallery/view/{slug}', 'ContributionController@viewGallery')->name('contribute.gallery.view');
	Route::get('/tag/view/{slug}', 'ContributionController@viewTag')->name('contribute.tag.view');

	Route::get('/gallery/request/view/{slug}', 'GalleryContributionController@galleryRequestEditView')->name('contribute.gallery-request.view');

	Route::get('/tag/request/view/{slug}', 'TagContributionController@tagRequestEditView')->name('contribute.tag-request.view');
});

// Route::get('/testing', 'ContributionController@uploadPost');

// Route::get('{path}','PagesController@browse')->where('path', '(.*)');

Route::post('/upload/post', 'ContributionController@fileUpload');

Route::group(['prefix' => 'api'], function(){
	Route::get('/get/galleries', 'GalleryController@apiGetGalleries');
	Route::post('/get/galleries/search', 'GalleryController@apiSearchGallery');
	Route::get('/get/tags/search', 'TagController@advanceSearchTag');
	Route::post('/get/tags/search', 'TagController@advanceSearchTag');


	Route::post('/get/user/ribbon', 'TagController@ajaxGetGalleryRibbon');

	Route::get('/test', 'TagController@test');

	Route::group(['prefix' => 'user'], function(){
		Route::post('/getUserTagCollection', 'UserController@getUserTagCollection');
		Route::post('/userTagCollection', 'UserController@userTagCollection');

		Route::post('/getUserGalleryCollection', 'UserController@getUserGalleryCollection');
		Route::post('/userGalleryCollection', 'UserController@userGalleryCollection');


		Route::post('/updateGalleryContribute', 'ContributionController@updateGalleryContribute');
		Route::post('/requestEditGallery', 'GalleryContributionController@requestEditGallery');
		Route::post('/requestEditModerateGallery', 'GalleryContributionController@requestEditModerateGallery');

		Route::post('/requestTagEdit', 'TagContributionController@requestTagEdit');
		Route::post('/updateRequestTagEdit', 'TagContributionController@updateRequestTagEdit');
	});

	Route::group(['prefix' => 'admin'], function(){
		Route::post('/moderateGalleryUpload', 'ContributionController@moderateGalleryUpload');

		Route::post('/moderateGalleryContribute', 'ContributionController@moderateGalleryContribute');
		Route::post('/moderateTagContribute', 'ContributionController@moderateTagContribute');
	});

});
