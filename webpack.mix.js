let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.autoload({ jQuery: 'jquery', $: 'jquery', jquery: 'jquery' });

mix.styles(['resources/assets/css/vendor/bootstrap.css'], 'public/assets/css/vendor.min.css')
	.styles(['resources/assets/css/app.css'], 'public/assets/css/app.min.css')
	.scripts([
		'resources/assets/js/vendor/carbon.js',
		'resources/assets/js/vendor/Chart.bundle.min.js',
	], 'public/assets/js/vendor.min.js');

mix.js('resources/assets/js/app.js', 'public/assets/js');
mix.styles(['resources/assets/css/vendor/carbon.min.css'], 'public/assets/css/admin.min.css');


mix.scripts([
	'resources/assets/js/admin.js'
], 'public/assets/js/admin.min.js');
