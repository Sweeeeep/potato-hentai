
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("vue-awesome-notifications/dist/styles/style.css")

require('./bootstrap');


require('./vendor/bootstrap');

require('./vendor/jquery.hoverIntent.min.js');

window.Vue = require('vue');
window.$ = window.jQuery = require('jquery');
import VueRouter from 'vue-router';

import Vue2Filters from 'vue2-filters'

import VeeValidate from 'vee-validate';

import VueTruncate from 'vue-truncate-filter';

import StarRating from 'vue-star-rating'

import Echo from "laravel-echo";

import VueAWN from "vue-awesome-notifications"

import BootstrapVue from 'bootstrap-vue'

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '407e31518469f5dc64d3',
	cluster: 'ap1',
    encrypted: true
});


Vue.use(BootstrapVue);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueAWN)

Vue.use(VueRouter);

Vue.use(Vue2Filters);

Vue.use(VeeValidate);

Vue.use(VueTruncate);

import bModal from 'bootstrap-vue/es/components/modal/modal';
import bModalDirective from 'bootstrap-vue/es/directives/modal/modal';

Vue.component('b-modal', bModal);
Vue.directive('b-modal', bModalDirective);

Vue.component('star-rating', StarRating);

Vue.component('example', require('./components/Example.vue'));
Vue.component('read-gallery', require('./components/GalleryRead.vue'));
Vue.component('upload-gallery', require('./components/UploadGallery.vue'));
Vue.component('notification', require('./components/Notification.vue'));

Vue.component('gallery', require('./components/GalleryList.vue'));

Vue.component('gallery-rating', require('./components/GalleryRating.vue'));

Vue.component('tag-user-control', require('./components/TagComponents/TagUserControl.vue'));
Vue.component('gallery-user-control', require('./components/Gallery/GalleryUserControl.vue'));

Vue.component('gallery-thumbnails', require('./components/Gallery/GalleryThumbImages.vue'));

Vue.component('add-tag', require('./components/Contribute/AddTag.vue'));

Vue.component('gallery-request-edit', require('./components/Gallery/GalleryRequestEdit.vue'));

// used

Vue.component('contribute-moderation', require('./components/Contribute/Moderation.vue'));

Vue.component('contribute-edit-gallery', require('./components/Contribute/GalleryEdit.vue'));

Vue.component('contribute-edit-tag', require('./components/Contribute/TagEdit.vue'));

// Vue.component('pagination', require('laravel-vue-pagination'));

// Admin Compnents


Vue.component('admin-gallery-update', require('./components/Admin/Gallery/update.vue'));



const routes = [
	{
		path: '/gallery/:slug/r/:page',
		name : 'readGallery'
	}
]

const router = new VueRouter({
	mode: 'history',
	routes,
});

const app = new Vue({
	router,
    el: '#app'
});

Vue.filter('formatSize', function (bytes) {
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];

})


$('li.dropdown.dropdown-notification').on('click', function (event) {
    $(this).toggleClass('show');
	$(this).find('.dropdown-menu').toggleClass('show')
});
$('.dropdown-menu').on('click', function (e) {
	 e.stopPropagation();
});
$(function() {
	var configHover = {
		over : function(){
			var anim = $(this).children('.card-footer');
			var height = $(this).children('.card-footer').outerHeight(true);
			anim.stop(true).delay(0).animate({
				top: '-'+height+'px'
			}, 300)
		},
		out : function(){
			var anim = $(this).children('.card-footer');
			anim.stop(true).delay(0).animate({
				top: '0px'
			}, 300);
		},
		timeout: 300,
		sensitivity: 1,
		interval: 100
	}
	$('.card.gallery').hoverIntent(configHover);

});
