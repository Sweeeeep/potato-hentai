export function ajaxFindTags (query, type) {
	return axios.post('/tags/search/' + type, {
		q: query
	});
}
export function ajaxSearchTags (query, check) {
	return axios.post('/api/get/tags/search', {
		q: query,
		c: check
	});
}
