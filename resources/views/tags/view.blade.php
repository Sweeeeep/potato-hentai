@extends('layouts.app')

@section('title', $tag->title. ' ( '. ucfirst($type) . ' ) ')

@section('content')

	<div class="jumbotron">
		<div class="pull-right">
			<tag-user-control :tag='{!! $tag->toJson() !!}'></tag-user-control>
			<contribute-edit-tag :type='1' :tag='{!! $tag->toJson() !!}'></contribute-edit-tag>
		</div>
		<h4>{{ $tag->title }} <br /> {{ $tag->j_title }}</h4>
		<p>
			<i>
				{{ $tag->alt_title }}
			</i>
		</p>
		<p>
			{{ $tag->description }}
		</p>
		<ul class="list-inline">
			<li class="list-inline-item">{{ $galleries->count() }} Results</li>
			<li class="list-inline-item">|</li>
			<li class="list-inline-item">Sort By</li>
			<li class="list-inline-item">
				<a href="{{ route('tag.view', [ 'type' => $type, 'slug' => $tag->slug, 'sort' => 'newest' ]) }}">Newest</a>
			</li>
			<li class="list-inline-item">
				<a href="{{ route('tag.view', [ 'type' => $type, 'slug' => $tag->slug, 'sort' => 'most-popular' ]) }}">Most Popular</a>
			</li>
			{{-- <li class="list-inline-item">
				<a href="">Highest Rated</a>
			</li> --}}
			<li class="list-inline-item">
				<a href="{{ route('tag.view', [ 'type' => $type, 'slug' => $tag->slug, 'sort' => 'most-viewed' ]) }}">Most Viewed</a>
			</li>
			<li class="list-inline-item">
				<a href="{{ route('tag.view', [ 'type' => $type, 'slug' => $tag->slug, 'sort' => 'title' ]) }}">Title</a>
			</li>
			<li class="list-inline-item">
				<a href="{{ route('tag.view', [ 'type' => $type, 'slug' => $tag->slug, 'sort' => 'random' ]) }}">Random</a>
			</li>
		</ul>
	</div>

	<div class="row" id="cards-padding">
		@php
			$check = PotatoHelper::getUserCollection($galleries->items());
		@endphp
		@foreach ($galleries as $gallery)
			@php
				if(isset($check[$gallery->id])){
					$card = PotatoHelper::filterCard($check[$gallery->id]);
				}
			@endphp
			<div class="card gallery zero-padding text-dark {{ isset($card['class']) ? $card['class'] : ''}}">
				@if(isset($card['class']) && isset($card['ribbonName']) && isset($card['ribbonClass']))
					<div class="ribbon {{ $card['ribbonClass'] }}">
						<span>
							{{ $card['ribbonName'] }}
						</span>
					</div>
				@endif
				<a href="{{ route('gallery.view', [ 'slug' => $gallery->slug ]) }}">
					<img class="card-img-top" src="{{ PotatoHelper::getCover($gallery->id) }}" alt="{{ $gallery->title }}">
					<div class="title-main">{{ $gallery->title }}<br/>{{ $gallery->j_title }}</div>
				</a>
				<div class="card-footer">
					<div class="meta-head">
						<div class="head-content">
							<div class="pull-right">
								@php
									$parodyTags = PotatoHelper::sortTagsByGroup($gallery->tags, 7);
									$done = false;
								@endphp
								@if(is_array($parodyTags))
									<span class="badge badge-text">
										@foreach ($parodyTags as $key => $tags)
											@foreach ($tags as $key => $tag)
												@if($tag->type == 7 && !$done)
													{{ $tag->title }}
													@php
														$done = true;
													@endphp
												@endif
											@endforeach
										@endforeach
									</span>
								@endif
							</div>
							<span class="badge badge-pages">{{ $gallery->page ? $gallery->page->total_pages : '?'}} Pages</span>
						</div>
					</div>
					<div class="meta-body">
						<div class="title"><a href="{{ route('gallery.view', [ 'slug' => $gallery->slug ]) }}">{{ $gallery->title }}<br/>{{ $gallery->j_title }}</a></div>
						@php
							$contentTags = PotatoHelper::sortTagsByGroup($gallery->tags, 2);
						@endphp
						@if(is_array($contentTags))
						<div class="meta-data">
							<label>Contents:</label>
							<ul>
								@foreach ($contentTags as $tags)
									@foreach (array_slice($tags, 0, 2) as $tag)
										@if($tag->type == 2)
										<li>
											<a href="">{{ $tag->title }}</a>
										</li>
									@endif
									@endforeach
								@endforeach
							</ul>
						</div>
					@endif
						@if($gallery->description)
							<div class="meta-data">
								<label>Summary:</label>
								<span>{{ str_limit($gallery->description, 100) }}</span>
							</div>
						@endif
					</div>
				</div>
			</div>
		@endforeach

		{{ $galleries->render() }}
	</div>

@endsection
