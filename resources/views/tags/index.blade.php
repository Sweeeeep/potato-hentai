@extends('layouts.app')

@section('title', 'Tags')

@section('content')
	<div class="jumbotron padding">
		All Tags
		<ul class="list-inline">
			<li class="list-inline-item">{{ $tags->count() }} Results</li>
			<li class="list-inline-item">|</li>
			<li class="list-inline-item">Sort By</li>
			<li class="list-inline-item list-bullet">
				<a href="{{ route('tag.index')}}" class="{{ !isset($type) ? 'disabled' : ''}}">All</a>
			</li>
			@foreach (PotatoHelper::$tagGroups as $key => $value)
				<li class="list-inline-item list-bullet">
					<a href="{{ route('tag.sort', [ 'type' => strtolower($value) ]) }}" class="{{ isset($type) && $type == $key ? 'disabled' : ''}}">{{ $value }}</a>
				</li>
			@endforeach

		</ul>
	</div>
	<div class="jumbotron padding">
		<table class="table table-striped">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="60%">Title</th>
					<th scope="col" width="20%">Type</th>
					<th scope="col" width="10%">Total</th>
					<th scope="col">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($tags as $tag)
					@php
						$type = PotatoHelper::$tagGroups[$tag->type];
					@endphp
					<tr>
						<td>{{ $tag->title }}</td>
						<td>{{ $type }}</td>
						<td>{{ $tag->gallery_count}}</td>
						<td><a href="{{ route('tag.view', ['type' => strtolower($type), 'slug' => $tag->slug, 'sort' => 'all']) }}"><b>Go</b></a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
		{{ $tags->render() }}
	</div>
@endsection
