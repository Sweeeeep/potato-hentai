<div class="col-md-4">
	<div class="list-group">
		<a href="{{ route('user.setting') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.setting') ? 'active' : '' }}">Account Overview</a>
		<a href="{{ route('user.setting.password') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.setting.password') ? 'active' : '' }}">Change Password</a>
		<a href="{{ route('user.setting.email') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.setting.email') ? 'active' : '' }}">Change Email</a>
		<a href="{{ route('user.setting.name') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.setting.name') ? 'active' : '' }}">Change Display Name</a>
	</div>
</div>
