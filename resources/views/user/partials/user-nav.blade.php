<div class="col-md-4">
	<div class="list-group">
		<a href="{{ route('user.auth.uploads') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.auth.uploads') ? 'active' : '' }}">Uploads</a>
		<a href="{{ route('user.auth.favorites') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.auth.favorites') ? 'active' : '' }}">Favorites</a>
		<a href="{{ route('user.auth.bookmarks') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.auth.bookmarks') ? 'active' : '' }}">Bookmarks</a>
		{{-- <a href="{{ route('user.setting.name') }}" class="list-group-item list-group-item-action {{ Route::currentRouteNamed('user.setting.name') ? 'active' : '' }}">Tag Collections</a> --}}
	</div>
</div>
