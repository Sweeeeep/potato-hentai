@extends('layouts.app')

@section('title', 'User Profile')


@section('content')
	<div class="jumbotron profile-title">
		<div class="pull-right">
			<a class="btn btn-primary btn-sm" href="{{ route('user.setting') }}">Setting</a>
			<a class="btn btn-primary btn-sm" href="{{ route('user.auth.uploads') }}">Collections</a>
		</div>
		<h3>{{ $user->getUserName() }}</h3>
		@foreach ($user->roles as $key => $role)
			<span class="roles" style="color:#{{ $role->color }};">{{ $role->name }}</span>
		@endforeach
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="jumbotron profile-title">
				@if(count($favorites) > 5)
					<div class="pull-right">
						<a class="btn btn-primary btn-sm" href="{{ route('user.auth.bookmarks') }}">View more</a>
					</div>
				@endif
				<h1>Recently Bookmark</h1>
				<hr />
				<table class="table table-user">
					<tbody>
						@foreach ($bookmarks as $ckey => $gallery)
							@if($ckey != 5)
								@php
									$gallery = $gallery->gallery;
								@endphp
								<tr>
									<td>#{{ $gallery->id }}</td>
									<td>
										<a href="{{ route('gallery.view', [
											'slug' => $gallery->slug
										])}}">
											{{ $gallery->getGalleryName() }}</td>
										</a>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="jumbotron profile-title">
				@if(count($favorites) > 5)
					<div class="pull-right">
						<a class="btn btn-primary btn-sm" href="{{ route('user.auth.favorites') }}">View more</a>
					</div>
				@endif
				<h1>Recently Favorite</h1>
				<hr />
				<table class="table table-user">
					<tbody>
						@foreach ($favorites as $ckey => $gallery)
							@if($ckey != 5)
								@php
									$gallery = $gallery->gallery;
								@endphp
								<tr>
									<td>#{{ $gallery->id }}</td>
									<td>
										<a href="{{ route('gallery.view', [
											'slug' => $gallery->slug
										])}}">
											{{ $gallery->getGalleryName() }}</td>
										</a>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-6">
			<div class="jumbotron profile-title">
				<div class="pull-right">
					<a class="btn btn-primary btn-sm" href="{{ route('user.auth.uploads') }}">View more</a>
				</div>
				<h1>Recently Uploads</h1>
				<hr />
				<table class="table table-user">
					<tbody>
						@foreach ($recentUpload as $key => $gallery)
							<tr>
								<td>#{{ $gallery->id }}</td>
								<td>
									<a href="{{ route('gallery.view', [
										'slug' => $gallery->slug
									])}}">
										{{ $gallery->getGalleryName() }}</td>
									</a>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="jumbotron profile-title">
				<h1>User Tags</h1>
				<hr />
				@foreach ($tagCollections as $key => $value)
					@if($key == 1)
						<h5>Liked Tags</h5>
						@foreach ($value as $key => $tag)
							<a href="{{ route('tag.view', [
								'type' => PotatoHelper::getTagGroupKey($tag->tags->type),
								'slug' => $tag->tags->slug
							])}}" class="badge badge-success">{{ $tag->tags->title }}</a>
						@endforeach
					@elseif($key == 2)
						<hr />
						<h5>Hated Tags</h5>
						@foreach ($value as $key => $tag)
							<a href="{{ route('tag.view', [
								'type' => PotatoHelper::getTagGroupKey($tag->tags->type),
								'slug' => $tag->tags->slug
							])}}" class="badge badge-danger">{{ $tag->tags->title }}</a>
						@endforeach
					@endif
				@endforeach
			</div>
		</div>
	</div>
@endsection
