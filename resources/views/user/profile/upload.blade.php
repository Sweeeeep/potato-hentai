@extends('layouts.app')

@section('title', 'User Uploads')

@section('content')
	<div class="row">
		@include('user.partials.user-nav')
		<div class="col-md-8">
			<div class="jumbotron normalize-padding">
				<table class="table">
					<thead>
						<tr>
							<th colspan="4" class="text-center">
								<h3>Uploads</h3>
							</th>
						</tr>
					</thead>
					<thead class="thead-dark">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Title</th>
							<th scope="col">Added at</th>
							<th scope="col">Link</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($uploads as $key => $gallery)
							<tr>
								<th scope="row">{{ $gallery->id }}</th>
								<td>{{ $gallery->getGalleryName() }}</td>
								<td>{{ $gallery->formatCreatedAt() }}</td>
								<td><a href="{{ route('gallery.view', [
									'slug' => $gallery->slug
								])}}">Go</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
				{{ $uploads->render() }}
			</div>
		</div>
	</div>
@endsection
