@extends('layouts.app')

@section('title', 'User Bookmark')

@section('content')
	<div class="row">
		@include('user.partials.user-nav')
		<div class="col-md-8">
			<div class="jumbotron normalize-padding">
				<table class="table">
					<thead>
						<tr>
							<th colspan="4" class="text-center">
								<h3>Bookmarks</h3>
							</th>
						</tr>
					</thead>
					<thead class="thead-dark">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Title</th>
							<th scope="col">Added at</th>
							<th scope="col">Link</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($bookmarks as $key => $galleryy)
							@php
								$gallery = $galleryy->gallery;
							@endphp
							<tr>
								<th scope="row">{{ $gallery->id }}</th>
								<td>{{ $gallery->getGalleryName() }}</td>
								<td>{{ $galleryy->created_at->format('M d, Y \a\t h:i') }}</td>
								<td><a href="{{ route('gallery.view', [
									'slug' => $gallery->slug
								])}}">Go</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
				{{ $bookmarks->render() }}
			</div>
		</div>
	</div>
@endsection
