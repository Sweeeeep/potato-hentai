@extends('layouts.app')

@section('title', 'Account Overview')

@section('content')
	<div class="row">
		@include('user.partials.setting-nav')
		<div class="col-md-8">
			<div class="jumbotron normalize-padding">
				<h1>Account Overview</h1>
				<hr />
				<table class="table table-striped">
					<tbody>
						<tr>
							<td width="25%">User ID</td>
							<td>{{ auth()->user()->id }}</td>
						</tr>
						<tr>
							<td>Display Name</td>
							<td>{{ auth()->user()->name }}</td>
						</tr>
						<tr>
							<td>Username</td>
							<td>{{ auth()->user()->name }}</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>{{ PotatoHelper::maskEmail(auth()->user()->email) }}</td>
						</tr>
						<tr>
							<td>Password</td>
							<td><a href="{{ route('user.setting.password') }}">Change Password</a></td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	</div>
@endsection
