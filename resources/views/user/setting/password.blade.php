@extends('layouts.app')

@section('title', 'Change Password')

@section('content')
	<div class="row">
		@include('user.partials.setting-nav')
		<div class="col-md-8">
			<div class="jumbotron normalize-padding">
				<h1>Change Password</h1>
				<hr />
				@if ($errors->any())
					<div class="alert alert-danger">
					    <ul class="zero-margin">
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
				@endif
				@include('flash::message')
				<form method="post">
					<div class="form-group">
						<label>Current Password</label>
						<input type="password" name="current_password" class="form-control"/>
					</div>
					<div class="form-group">
						<label>New Password</label>
						<input type="password" name="password" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Confirm New Password</label>
						<input type="password" name="password_confirmation" class="form-control"/>
					</div>
					<div class="form-group">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-outline-primary">Change!</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
