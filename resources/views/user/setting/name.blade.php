@extends('layouts.app')

@section('title', 'Change Display Name')

@section('content')
	<div class="row">
		@include('user.partials.setting-nav')
		<div class="col-md-8">
			<div class="jumbotron normalize-padding">
				<h1>Change Display Name</h1>
				<hr />
				@if ($errors->any())
					<div class="alert alert-danger">
					    <ul class="zero-margin">
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
				@endif
				@include('flash::message')
				<form method="post">
					@php
						$name = auth()->user()->name;
					@endphp
					<div class="form-group">
						<div class="alert alert-info text-center">
							Your current display name : <b>{{ auth()->user()->getUserName() }}</b><br/>
							You can only change name once so keep it unique and enough to become popular
						</div>
					</div>
					<div class="form-group">
						<label>Desire Name</label>
						<input type="text" name="name" class="form-control" value="{{ $name ? $name : old('name') }}" {{ empty($name) ? '' : 'disabled'}}/>
					</div>
					<div class="form-group">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-outline-primary" {{ empty($name) ? '' : 'disabled'}}>Set!</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
