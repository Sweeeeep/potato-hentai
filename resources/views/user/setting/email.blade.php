@extends('layouts.app')

@section('title', 'Change Email')

@section('content')
	<div class="row">
		@include('user.partials.setting-nav')
		<div class="col-md-8">
			<div class="jumbotron normalize-padding">
				<h1>Change Email</h1>
				<hr />
				@if ($errors->any())
					<div class="alert alert-danger">
					    <ul class="zero-margin">
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
					</div>
				@endif
				@include('flash::message')
				<form method="post">
					<div class="form-group">
						<label>Current Email</label>
						<input type="email" name="current_email" class="form-control" value="{{ old('current_email') }}"/>
					</div>
					<div class="form-group">
						<label>New Email</label>
						<input type="email" name="email" class="form-control"/>
					</div>
					<div class="form-group">
						<label>Confirm New Email</label>
						<input type="email" name="email_confirmation" class="form-control"/>
					</div>
					<div class="form-group">
						{{ csrf_field() }}
						<button type="submit" class="btn btn-outline-primary">Change!</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
