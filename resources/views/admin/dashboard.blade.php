@extends('layouts.admin')

@section('title', 'Dashboard')


@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="card p-4">
            <div class="card-body d-flex justify-content-between align-items-center">
                <div>
                    <span class="h4 d-block font-weight-normal mb-2">{{ $user }}</span>
                    <span class="font-weight-light">Total Users</span>
                </div>

                <div class="h2 text-muted">
                    <i class="icon icon-people"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="card p-4">
            <div class="card-body d-flex justify-content-between align-items-center">
                <div>
                    <span class="h4 d-block font-weight-normal mb-2">{{ $gallery }}</span>
                    <span class="font-weight-light">Total Galleries</span>
                </div>

                <div class="h2 text-muted">
                    <i class="icon icon-layers"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="card p-4">
            <div class="card-body d-flex justify-content-between align-items-center">
                <div>
                    <span class="h4 d-block font-weight-normal mb-2">{{ $tag }}</span>
                    <span class="font-weight-light">Total Tags</span>
                </div>

                <div class="h2 text-muted">
                    <i class="icon icon-tag"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="card p-4">
            <div class="card-body d-flex justify-content-between align-items-center">
                <div>
                    <span class="h4 d-block font-weight-normal mb-2">{{ $contribute }}</span>
                    <span class="font-weight-light">Total Contributions</span>
                </div>

                <div class="h2 text-muted">
                    <i class="icon icon-star"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row ">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                Total Users
            </div>

            <div class="card-body p-0">
                <div class="p-4">
                    <canvas id="dashboardUser-chart" width="100%" height="20"></canvas>
                </div>

                {{-- <div class="justify-content-around mt-4 p-4 bg-light d-flex border-top d-md-down-none">
                    <div class="text-center">
                        <div class="text-muted small">Total Traffic</div>
                        <div>12,457 Users (40%)</div>
                    </div>

                    <div class="text-center">
                        <div class="text-muted small">Banned Users</div>
                        <div>95,333 Users (5%)</div>
                    </div>

                    <div class="text-center">
                        <div class="text-muted small">Page Views</div>
                        <div>957,565 Pages (50%)</div>
                    </div>

                    <div class="text-center">
                        <div class="text-muted small">Total Downloads</div>
                        <div>957,565 Files (100 TB)</div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
	<script>
		let userChart = @json($chartData)
	</script>
@endsection
