@extends('layouts.admin')

@section('title', 'Galleries')

@section('content')
	<div class="card">
        <div class="card-header bg-light">
            Galleries
        </div>

        <div class="card-body">
			@include('flash::message')
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Name</th>
							<th>Uploaded by</th>
							<th>Created Date</th>
	                        <th>Action</th>
	                    </tr>
                    </thead>
                    <tbody>
						@foreach ($galleries as $key => $gallery)
		                    <tr>
		                        <td>{{ $gallery->id }}</td>
		                        <td class="text-nowrap">{{ $gallery->getGalleryName() }}</td>
								<td>{{ $gallery->user->getUserName() }}</td>
								<td>{{ $gallery->created_at->format('M d, Y \a\t h:i')}}</td>
								<td>
									<a class="btn btn-primary btn-sm" href="{{ route('gallery.view', [
										'slug' => $gallery->slug
									]) }}" data-toggle="tooltip" data-placement="top" title="View live"><i class="fa fa-eye"></i></a>
									<a class="btn btn-success btn-sm" href="{{ route('admin.gallery.update', [
										'id' => $gallery->id
									])}}" data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-wrench"></i></a>
									@if($gallery->status == 8)
										<a class="btn btn-warning btn-sm" href="{{ route('admin.gallery.recover', [
											'id' => $gallery->id
										]) }}" data-toggle="tooltip" data-placement="top" title="Recover" onclick="return confirm('Are you sure you want to recover?')"><i class="fa fa-cloud-upload-alt"></i></a>
									@else
										<a class="btn btn-danger btn-sm" href="{{ route('admin.gallery.delete', [
											'id' => $gallery->id
										]) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-cloud-download-alt" onclick="return confirm('Are you sure you want to delete?')"></i></a>
									@endif
								</td>
		                    </tr>
						@endforeach
                    </tbody>
                </table>
				{{ $galleries->render() }}
            </div>
        </div>
    </div>
@endsection
