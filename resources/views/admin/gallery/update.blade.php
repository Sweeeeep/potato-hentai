@extends('layouts.admin')

@section('title', 'Update Gallery '. $gallery->getGalleryName())

@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header bg-light">
	           Update Gallery '{{ $gallery->getGalleryName() }}'
	        </div>
			<div class="card-body">
				<admin-gallery-update :gallery='{{ json_encode($gallery->only('id', 'title', 'j_title', 'description')) }}' :tags='@json($sortedTags)' :language='@json($languages)' :category='@json($category)'></admin-gallery-update>
			</div>
		</div>
	</div>
</div>
@endsection
