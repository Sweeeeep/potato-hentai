@extends('layouts.admin')

@section('title', 'Update Tag '.$tag->title)

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header bg-light">
	           Update Tag '{{ $tag->title }}'
	        </div>
			<div class="card-body">
				@include('partials.alert-messages')
				<form method="post">
					<div class="row">
						<div class="form-group col-md-6">
							<label>English</label>
							<input type="text" class="form-control" name="title" placeholder="English" value="{{ $tag->title }}">
						</div>
						<div class="form-group col-md-6">
							<label>Japanese</label>
							<input type="text" class="form-control" name="j_title" placeholder="Japanese" value="{{ $tag->j_title }}">
						</div>
						<div class="form-group col-md-6">
							<label>Alternative Title</label>
								<input type="text" class="form-control" name="alt_title" placeholder="Alternative Title" value="{{ $tag->alt_title }}">
						</div>
						<div class="form-group col-md-6">
							<label>Type</label>
							<select class="form-control" name="type">
								@foreach (PotatoHelper::$tagGroups as $key => $value)
									<option value="{{ $key }}" {{ $tag->type == $key ? 'selected' : ''}}>
										{{ $value }}
									</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-12">
							<label>Description</label>
							<textarea class="form-control" name="description">{{ $tag->description }}</textarea>
						</div>


						<div class="form-group col-md-12">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
		            </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
