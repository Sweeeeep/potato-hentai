@extends('layouts.admin')

@section('title', 'Tags')

@section('content')
	<div class="card">
        <div class="card-header bg-light">
            Tags
        </div>

        <div class="card-body">
			@include('flash::message')
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Name</th>
							<th>Uploaded by</th>
							<th>Created Date</th>
	                        <th>Action</th>
	                    </tr>
                    </thead>
                    <tbody>
						@foreach ($tags as $key => $tag)
		                    <tr>
		                        <td>{{ $tag->id }}</td>
		                        <td class="text-nowrap">{{ $tag->title }}</td>
								<td>{{ $tag->user->getUserName() }}</td>
								<td>{{ $tag->created_at->format('M d, Y \a\t h:i')}}</td>
								<td>
									<a class="btn btn-primary btn-sm" href="{{ route('tag.view', [
										'type' => PotatoHelper::getTagGroupKey($tag->type),
										'slug' => $tag->slug
									]) }}" data-toggle="tooltip" data-placement="top" title="View live"><i class="fa fa-eye"></i></a>
									<a class="btn btn-success btn-sm" href="{{ route('admin.tag.update', [
										'id' => $tag->id
									])}}" data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-wrench"></i></a>
									@if($tag->status == 8)
										<a class="btn btn-warning btn-sm" href="{{ route('admin.tag.recover', [
											'id' => $tag->id
										]) }}" data-toggle="tooltip" data-placement="top" title="Recover" onclick="return confirm('Are you sure you want to recover?')"><i class="fa fa-cloud-upload-alt"></i></a>
									@else
										<a class="btn btn-danger btn-sm" href="{{ route('admin.tag.delete', [
											'id' => $tag->id
										]) }}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-cloud-download-alt" onclick="return confirm('Are you sure you want to delete?')"></i></a>
									@endif
								</td>
		                    </tr>
						@endforeach
                    </tbody>
                </table>
				{{ $tags->render() }}
            </div>
        </div>
    </div>
@endsection
