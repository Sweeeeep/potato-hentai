@extends('layouts.admin')

@section('title', 'Edit User '. $user->getUserName())

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header bg-light">
	            Edit User {{ $user->getUserName() }}
	        </div>
			<div class="card-body">
				@include('partials.alert-messages')
				<form method="post">
					<div class="row">
						<div class="form-group col-md-12">
			                <label>ID</label>
			                <div class="input-group mb-3">
			                    <div class="input-group-prepend">
			                        <span class="input-group-text"><i class="fa fa-hashtag"></i></span>
			                    </div>
			                    <input type="text" class="form-control" placeholder="ID" value="{{ $user->id }}" readonly>
			                </div>
						</div>

						<div class="form-group col-md-6">
							<label>Username</label>
			                <div class="input-group mb-3">
			                    <div class="input-group-append">
			                        <span class="input-group-text"><i class="fa fa-user"></i></span>
			                    </div>
								<input type="text" class="form-control" placeholder="Username" value="{{ $user->username }}" readonly>
							</div>
		                </div>

						<div class="form-group col-md-6">
							<label>Display Name</label>
							<div class="input-group mb-3">
			                    <div class="input-group-append">
			                        <span class="input-group-text"><i class="fa fa-eye"></i></span>
			                    </div>
								<input type="text" class="form-control" name="display_name" placeholder="Display Name" value="{{ $user->name }}">
							</div>
						</div>

						<div class="form-group col-md-12">
							<label>Update Password</label>
							<div class="input-group mb-3">
			                    <div class="input-group-append">
			                        <span class="input-group-text"><i class="fa fa-lock"></i></span>
			                    </div>
								<input type="text" class="form-control" name="password"  placeholder="Leave it empty">
							</div>
						</div>
						<div class="form-group col-md-12">
							<label>Role</label>
							<select class="form-control" name="role[]" multiple>
								@foreach ($roles as $key => $role)
									<option value="{{ $role->id}}" {{ $user->hasRole($role->name) ? 'selected' : ''}}>
										{{ $role->name }}
									</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-md-12">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
		            </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
