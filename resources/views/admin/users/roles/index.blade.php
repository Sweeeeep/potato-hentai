@extends('layouts.admin')

@section('title', 'Roles')

@section('content')
	<div class="card">
        <div class="card-header bg-light">
            Roles
			<div class="pull-right">
				<a href="{{ route('admin.user.roles.create') }}" class="btn btn-sm btn-primary">Create</a>
			</div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>Name</th>
							<th>Created Date</th>
	                        <th>Action</th>
	                    </tr>
                    </thead>
                    <tbody>
						@foreach ($roles as $key => $role)
		                    <tr>
		                        <td>{{ $role->id }}</td>
		                        <td class="text-nowrap">{{ $role->name }}</td>
								<td>{{ $role->created_at->format('M d, Y \a\t h:i')}}</td>
								<td>
									<a href="{{ route('admin.user.roles.update', [
										'id' => $role->id
										]) }}" class="btn btn-primary btn-sm"><i class="fa fa-wrench"></i></a>
								</td>
		                    </tr>
						@endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
