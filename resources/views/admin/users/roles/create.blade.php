@extends('layouts.admin')

@section('title', 'Create new Role')

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header bg-light">
	           Create new Role
	        </div>
			<div class="card-body">
				@include('partials.alert-messages')
				<form method="post">
					<div class="row">
						<div class="form-group col-md-12">
							<label>Name</label>
							<div class="input-group mb-3">
			                    <div class="input-group-append">
			                        <span class="input-group-text"><i class="fa fa-eye"></i></span>
			                    </div>
								<input type="text" class="form-control" name="name" placeholder="Role Name" value="">
							</div>
						</div>

						<div class="form-group col-md-12">
							<label>Permissions</label>
							<select class="form-control" name="permissions[]" multiple>
								@foreach ($permissions as $key => $permission)
									<option value="{{ $permission->name }}">
										{{ $permission->name }}
									</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-md-12">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary">Create</button>
						</div>
		            </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
