@extends('layouts.admin')

@section('title', 'Edit Role '. $role->name)

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header bg-light">
	            Edit Role {{ $role->name }}
	        </div>
			<div class="card-body">
				@include('partials.alert-messages')
				<form method="post">
					<div class="row">
						<div class="form-group col-md-6">
			                <label>ID</label>
			                <div class="input-group mb-3">
			                    <div class="input-group-prepend">
			                        <span class="input-group-text"><i class="fa fa-hashtag"></i></span>
			                    </div>
			                    <input type="text" class="form-control" placeholder="ID" value="{{ $role->id }}" readonly>
			                </div>
						</div>

						<div class="form-group col-md-6">
							<label>Name</label>
							<div class="input-group mb-3">
			                    <div class="input-group-append">
			                        <span class="input-group-text"><i class="fa fa-eye"></i></span>
			                    </div>
								<input type="text" class="form-control" name="name" placeholder="Role Name" value="{{ $role->name }}">
							</div>
						</div>

						<div class="form-group col-md-12">
							<label>Permissions</label>
							<select class="form-control" name="permissions[]" multiple>
								@foreach ($permissions as $key => $permission)
									<option value="{{ $permission->name }}" {{ $role->hasPermissionTo($permission->name) ? 'selected' : ''}}>
										{{ $permission->name }}
									</option>
								@endforeach
							</select>
						</div>

						<div class="form-group col-md-12">
							{{ csrf_field() }}
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
		            </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
