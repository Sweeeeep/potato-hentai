@extends('layouts.admin')

@section('title', 'Users')

@section('content')
	<div class="card">
        <div class="card-header bg-light">
            Users
        </div>

        <div class="card-body">
			<form>
				<div class="form-group">
					<div class="input-group">
						<input type="text" name="search_query" class="form-control" placeholder="Search" value="{{ old('search_query') }}">
						<span class="input-group-btn">
							<button type="button" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
						</span>
					</div>
				</div>
			</form>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Display Name</th>
                        <th>Username</th>
                        <th>Email</th>
						<th>Joined Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
						@foreach ($users as $key => $user)
		                    <tr>
		                        <td>{{ $user->id }}</td>
		                        <td class="text-nowrap">{{ $user->getUserName() }}</td>
		                        <td>{{ $user->username}}</td>
		                        <td>{{ $user->email }}</td>
								<td>{{ $user->created_at->format('M d, Y \a\t h:i')}}</td>
								<td>
									<a href="{{ route('admin.user.update', [
										'id' => $user->id
										]) }}" class="btn btn-primary btn-sm"><i class="fa fa-wrench"></i></a>
								</td>
		                    </tr>
						@endforeach
                    </tbody>
                </table>
				{{ $users->render() }}
            </div>
        </div>
    </div>
@endsection
