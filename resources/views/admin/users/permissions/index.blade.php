@extends('layouts.admin')

@section('title', 'Permissions')

@section('content')
	<div class="card">
        <div class="card-header bg-light">
            Permissions
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
						<th>Created Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
						@foreach ($permissions as $key => $permission)
		                    <tr>
		                        <td>{{ $permission->id }}</td>
		                        <td class="text-nowrap">{{ $permission->name }}</td>
								<td>{{ $permission->created_at->format('M d, Y \a\t h:i')}}</td>
								<td>
									<a href="{{ route('admin.user.permissions.update', [
										'id' => $permission->id
										]) }}" class="btn btn-primary btn-sm"><i class="fa fa-wrench"></i></a>
								</td>
		                    </tr>
						@endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
