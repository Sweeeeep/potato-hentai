@extends('layouts.app')


@section('title', 'Edit for '.$gallery->getGalleryName())

@section('content')
	<div class="jumbotron">
		<div class="row">
			<div class="col-md-12">
				<table class="table table-gallery-info">
					<tbody>
						<tr>
							<td colspan="3" class="text-center">
								<h4>Edit for <i>{{ $gallery->getGalleryName() }}</i></h4>
							</td>
						</tr>
						@if($gallery->status == 10)
							<tr>
								<td width="10%"></td>
								<td width="45%">Current</td>
								<td width="45%">Suggested</td>
							</tr>
						@else
							<tr>
								<td width="10%"></td>
								<td width="45%">Old</td>
								<td width="45%">New</td>
							</tr>
						@endif
						<tr>
							<td>
								English
							</td>
							<td>
								{{ $gallery->gallery->title }}
							</td>
							<td>
								{{ $gallery->title }}
							</td>
						</tr>
						<tr>
							<td>
								Japanese
							</td>
							<td>
								{{ $gallery->gallery->j_title }}
							</td>
							<td>
								{{ $gallery->j_title }}
							</td>
						</tr>
						<tr>
							<td>
								Description
							</td>
							<td>
								{{ $gallery->gallery->description }}
							</td>
							<td>
								{{ $gallery->description }}
							</td>
						</tr>

						@if($gallery->status == 10)
							@php
								$current = PotatoHelper::sortTagsByGroup($gallery->gallery->tags);
								$new = PotatoHelper::sortTagsByGroup($gallery->tags);
							@endphp
							@foreach ($current as $group => $oldTags)
								@foreach ($new as $key => $newTags)
									@if($group == $key)
										@php
										 	$currentTags = PotatoHelper::getTagID($oldTags);
											$checkNew = PotatoHelper::getTagID($newTags);
										@endphp

										<tr>
											<td>{{ $group }}</td>
											<td>
												@if(count($oldTags))
													<ul class="list-inline">
														@foreach ($oldTags as $key => $oldTag)
															<li>

																<a href="{{ route('tag.view', ['type' => strtolower($group), 'slug' => $oldTag->slug]) }}" class="{{ !in_array($oldTag->id, $checkNew) ? 'text-danger' : ''}}">

																	{{ $oldTag->title }}
																</a>
															</li>
														@endforeach
													</ul>
												@else
													-
												@endif
											</td>
											<td>
												@if(count($newTags))
													<ul class="list-inline">
														@foreach ($newTags as $key => $newTag)
															<li>
																<a class="{{ !in_array($newTag->id, $currentTags) ? 'text-success' : ''}}" href="{{ $newTag->status == 10 ? '#' : route('tag.view', ['type' => strtolower($group), 'slug' => $newTag->slug]) }}">
																	{{ $newTag->title }}
																	{{-- {{ $newTag->status == 10 ? '(new)' : ''}} --}}
																</a>
															</li>
														@endforeach
													</ul>
												@else
													-
												@endif
											</td>
										</tr>
									@endif
								@endforeach
							@endforeach
						@else
							@php
								$new = PotatoHelper::sortTagsByGroup($gallery->gallery->tags);
								$current = PotatoHelper::sortTagsByGroup($gallery->tags);
							@endphp
							@foreach ($current as $group => $oldTags)
								@foreach ($new as $key => $newTags)
									@if($group == $key)
										@php
											$currentTags = PotatoHelper::getTagID($oldTags);
											$checkNew = PotatoHelper::getTagID($newTags);
										@endphp

										<tr>
											<td>{{ $group }}</td>
											<td>
												@if(count($oldTags))
													<ul class="list-inline">
														@foreach ($oldTags as $key => $oldTag)
															<li>

																<a href="{{ route('tag.view', ['type' => strtolower($group), 'slug' => $oldTag->slug]) }}" class="{{ !in_array($oldTag->id, $checkNew) ? 'text-danger' : ''}}">

																	{{ $oldTag->title }}
																</a>
															</li>
														@endforeach
													</ul>
												@else
													-
												@endif
												<td>
													@if(count($newTags))
														<ul class="list-inline">
															@foreach ($newTags as $key => $newTag)
																<li>
																	<a class="{{ !in_array($newTag->id, $currentTags) ? 'text-success' : ''}}" href="{{ $newTag->status == 10 ? '#' : route('tag.view', ['type' => strtolower($group), 'slug' => $newTag->slug]) }}">
																		{{ $newTag->title }}
																		{{-- {{ $newTag->status == 10 ? '(new)' : ''}} --}}
																	</a>
																</li>
															@endforeach
														</ul>
													@else
														-
													@endif
												</td>
											</td>
										</tr>
									@endif
								@endforeach
							@endforeach
						@endif
					</tbody>
				</table>
				<div class="row">

				<div class="col-md-6">
					<table class="table table-gallery-info">
						<tbody>
							<tr>
								<td colspan="2">
									<h5 class="text-danger">Contribution Information</h5>
								</td>
							</tr>
							<tr>
								<td width="50%">
									Submitted By
								</td>
								<td>
									<a href="">{{ $gallery->user->getUserName() }}</a>
								</td>
							</tr>
							<tr>
								<td>
									Submitted on
								</td>
								<td>
									{{ $gallery->created_at->format('M d, Y \a\t h:m') }}
								</td>
							</tr>
							<tr>
								<td>
									Last updated
								</td>
								<td>
									{{ $gallery->created_at->format('M d, Y \a\t h:m') }}
								</td>
							</tr>
							<tr>
								<td>
									Status
								</td>
								<td>
									{{ $gallery->getStatus() }}
								</td>
							</tr>
							<tr>
								<td>
									Note
								</td>
								<td>
									{{ $gallery->getStatus() }}
								</td>
							</tr>
							<tr>
								<td>
									Gallery
								</td>
								<td>
									<a href="{{ route('gallery.view', [ 'slug' => $gallery->gallery->slug ]) }}">
										{{ $gallery->gallery->getGalleryName() }}
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-6 align-self-center text-center">
					<a class="btn btn-outline-success">Like</a>
					<a class="btn btn-outline-success">Dislike</a>
					<hr/>
					<div>
						<contribute-moderation :id="{{ $gallery->id }}" :type='1' :tags='{{ PotatoHelper::checkTagStatus($gallery->tags) ? 'true' : 'false' }}'></contribute-moderation>
						<contribute-edit-gallery :gallery="{{ json_encode($gallery->only('id', 'title', 'j_title', 'description')) }}" :tags="{{ json_encode($new) }}" :language="{{ json_encode($languages) }}" :category="{{ json_encode($category) }}" :type='3'></contribute-edit-gallery>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
@endsection
