@extends('layouts.app')

@section('title', $tag->getTagName())

@section('content')
	<div class="jumbotron">
		<div class="row">
			<div class="col-md-6">
				<table class="table table-gallery-info">
					<tbody>
						<tr>
							<td colspan="3">
								<h4>New Tag Information</h4>
							</td>
						</tr>
						<tr>
							<td width="25%"></td>
							<td width="35%">Current</td>
							<td width="35%">Suggested</td>
						</tr>
						<tr>
							<td>
								English
							</td>
							<td>
								{{ $tag->tag->title or '-' }}
							</td>
							<td>
								{{ $tag->title or '-' }}
							</td>
						</tr>
						<tr>
							<td>
								Japanese
							</td>
							<td>
								{{ $tag->tag->j_title or '-' }}
							</td>
							<td>
								{{ $tag->j_title or '-' }}
							</td>
						</tr>
						<tr>
							<td>
								Alternative Title
							</td>
							<td>
								{{ $tag->tag->alt_title or '-' }}
							</td>
							<td>
								{{ $tag->alt_title or '-' }}
							</td>
						</tr>
						<tr>
							<td>
								Description
							</td>
							<td>
								{{ $tag->tag->description or '-' }}
							</td>
							<td>
								{{ $tag->description or '-' }}
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<h4>Contribution Information</h4>
							</td>
						</tr>
						<tr>
							<td width="50%">
								Submitted By
							</td>
							<td>
								<a href="">{{ $tag->user->getUserName() }}</a>
							</td>
						</tr>
						<tr>
							<td>
								Submitted on
							</td>
							<td>
								{{ $tag->created_at->format('M d, Y \a\t h:m') }}
							</td>
						</tr>
						<tr>
							<td>
								Last updated
							</td>
							<td>
								{{ $tag->created_at->format('M d, Y \a\t h:m') }}
							</td>
						</tr>
						<tr>
							<td>
								Status
							</td>
							<td>
								{{ $tag->getStatus() }}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-12">
				@if($tag->status == 10)
					<contribute-moderation :id="{{ $tag->id }}" :type='4' :tags='false'></contribute-moderation>
					<contribute-edit-tag :type='3' :tag='{{ json_encode($tag->only('id', 'user_id', 'title', 'j_title', 'alt_title', 'description')) }}'></contribute-edit-tag>
				@endif
			</div>
		</div>
	</div>
@endsection
