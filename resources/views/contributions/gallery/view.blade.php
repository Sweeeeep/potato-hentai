@extends('layouts.app')

@section('title', $gallery->title)

@section('content')
	<div class="jumbotron">
		<div class="row">
			<div class="col-md-3 col-sm-12 text-center">
				<img style="width:100%;" class="gallery-cover" src="{{ PotatoHelper::getCover($gallery->id) }}" />
				@if($gallery->status == 10)
					<a href="#" class="btn btn-outline-primary btn-strong">Like</a>
					<a href="#" class="btn btn-outline-primary btn-strong">Dislike</a>
				@endif
			</div>
			<div class="col-md-9 col-sm-12">
				<table class="table table-gallery-info contribute">
					<tbody>
						<tr>
							<td colspan="2">
								<h4>{{ $gallery->title }} / {{ $gallery->j_title }}</h4>
							</td>
						</tr>
						<tr>
							<td>
								Alternative Title
							</td>
							<td>
								{{ $gallery->alt_title }}
							</td>
						</tr>
						@php
							$haveNewTag = false;
						@endphp
						@if(is_array($sortedTags))
							@foreach ($sortedTags as $group => $tags)
									<tr>
										<td>{{ $group }}</td>
										<td>
											@if(count($tags))
												<ul class="list-inline">
													@foreach ($tags as $value)
														@if($value->status == 10)
															@php
																$haveNewTag = true;
															@endphp
														@endif
														<li>
															<a class="{{ $value->status == 10 ? 'new-tag' : ''}}" href="{{ $value->status == 10 ? '#' : route('tag.view', ['type' => strtolower($group), 'slug' => $value->slug]) }}">{{ $value->title }}{{ $value->status == 10 ? '(new)' : ''}}</a>
														</li>
													@endforeach
												</ul>
											@else
												-
											@endif
										</td>
									</tr>
							@endforeach
						@endif
						<tr>
							<td>
								Pages
							</td>
							<td>
								{{ $gallery->page ? $gallery->page->total_pages : '0' }}
							</td>
						</tr>
						<tr>
							<td>
								Description
							</td>
							<td>
								{{ $gallery->description or '-' }}
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<h5 class="text-danger">Contribution Information</h5>
							</td>
						</tr>
						<tr>
							<td>
								Submitted by
							</td>
							<td>
								<a href="">{{ $gallery->user->getUserName() }}</a>
							</td>
						</tr>
						<tr>
							<td>
								Submitted on
							</td>
							<td>
								{{ $gallery->created_at->format('M d, Y \a\t h:i') }}
							</td>
						</tr>
						<tr>
							<td>
								Last updated
							</td>
							<td>
								{{ $gallery->updated_at->format('M d, Y \a\t h:i') }}
							</td>
						</tr>
						<tr>
							<td>
								Status
							</td>
							<td>
								{{ $gallery->getStatus() }}
							</td>
						</tr>
						<tr>
							<td>
								Note
							</td>
							<td>
								{{ $gallery->getStatus() }}
							</td>
						</tr>
					</tbody>
				</table>
				@if($gallery->status == 10)
					<contribute-moderation :id="{{ $gallery->id }}" :type='3' :tags='{{ PotatoHelper::checkTagStatus($gallery->tags) ? 'true' : 'false' }}'></contribute-moderation>
					@if(Auth::check())
						<contribute-edit-gallery :gallery="{{ json_encode($gallery->only('id', 'title', 'j_title', 'description')) }}" :tags="{{ json_encode($sortedTags) }}" :language="{{ json_encode($languages) }}" :category="{{ json_encode($category) }}" :type='2'></contribute-edit-gallery>
					@endif
				@endif
			</div>
		</div>
	</div>
@endsection
