@extends('layouts.app')

@section('title', 'Pending User Contributions List')

@section('content')
	<div class="jumbotron padding">
		<h3>Pending User Contributions</h3>
		<ul class="list-inline">
			<li class="list-inline-item"> Results</li>
			<li class="list-inline-item">|</li>
			<li class="list-inline-item">Sort By</li>
		</ul>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th colspan="4" class="text-center">
					<h3>Uploads Contributions</h3>
				</th>
			</tr>
		</thead>
		<thead class="thead-dark">
			<tr>
				<th width="50%">Title</th>
				<th width="15%">Submitted by</th>
				<th width="20%">Submitted at</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($galleries as $key => $gallery)
				<tr>
					<th scope="row">
						<a href="{{ route('contribute.gallery.view', ['slug' => $gallery->slug]) }}">
							{{ $gallery->getGalleryName() }}
						</a>
					</th>
					<td>{{ $gallery->user->getUserName() }}</td>
					<td>{{ $gallery->created_at->format('M d, Y \a\t h:i') }}</td>
					<td>{{ $gallery->getStatus() }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<table class="table table-striped">
		<thead>
			<tr>
				<th colspan="4" class="text-center">
					<h3>Tags Contributions</h3>
				</th>
			</tr>
		</thead>
		<thead class="thead-dark">
			<tr>
				<th width="50%">Title</th>
				<th width="15%">Submitted by</th>
				<th width="20%">Submitted at</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($tags as $key => $tag)
				<tr>
					<th scope="row">
						<a href="{{ route('contribute.tag.view', ['slug' => $tag->slug]) }}">
							{{ $tag->getTagName() }}
						</a>
					</th>
					<td>{{ $tag->user->getUserName() }}</td>
					<td>{{ $tag->created_at->format('M d, Y \a\t h:i') }}</td>
					<td>{{ $tag->getStatus() }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<table class="table table-striped">
		<thead>
			<tr>
				<th colspan="4" class="text-center">
					<h3>Edit Gallery Contributions</h3>
				</th>
			</tr>
		</thead>
		<thead class="thead-dark">
			<tr>
				<th width="50%">Title</th>
				<th width="15%">Submitted by</th>
				<th width="20%">Submitted at</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($editGalleries as $key => $gallery)
				<tr>
					<th scope="row">
						<a href="{{ route('contribute.gallery-request.view', ['slug' => $gallery->slug]) }}">
							{{ $gallery->getGalleryName() }}
						</a>
					</th>
					<td>{{ $gallery->user->getUserName() }}</td>
					<td>{{ $gallery->created_at->format('M d, Y \a\t h:i') }}</td>
					<td>{{ $gallery->getStatus() }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	<table class="table table-striped">
		<thead>
			<tr>
				<th colspan="4" class="text-center">
					<h3>Edit Tags Contributions</h3>
				</th>
			</tr>
		</thead>
		<thead class="thead-dark">
			<tr>
				<th width="50%">Title</th>
				<th width="15%">Submitted by</th>
				<th width="20%">Submitted at</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($editTags as $key => $tag)
				<tr>
					<th scope="row">
						<a href="{{ route('contribute.tag-request.view', ['slug' => $tag->slug]) }}">
							{{ $tag->getTagName() }}
						</a>
					</th>
					<td>{{ $tag->user->getUserName() }}</td>
					<td>{{ $tag->created_at->format('M d, Y \a\t h:i') }}</td>
					<td>{{ $tag->getStatus() }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection
