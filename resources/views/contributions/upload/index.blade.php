@extends('layouts.app')

@section('title', 'Upload')

@section('content')

	<upload-gallery :category='{!! $category->toJson() !!}' :language='{!! $languages->toJson() !!}'></upload-gallery>

@endsection
