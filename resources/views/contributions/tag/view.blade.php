@extends('layouts.app')

@section('title', 'Edit for '.$tag->getTagName())

@section('content')

	<div class="jumbotron">
		<div class="row">
			<div class="col-md-9 col-sm-12">
			<table class="table table-gallery-info">
				<tbody>
					<tr>
						<td colspan="2">
							<h4>New Tag Information</h4>
						</td>
					</tr>
					<tr>
						<td>
							English
						</td>
						<td>
							{{ $tag->title }}
						</td>
					</tr>
					<tr>
						<td>
							Japanese
						</td>
						<td>
							{{ $tag->j_title or '-' }}
						</td>
					</tr>
					<tr>
						<td>
							Alternative
						</td>
						<td>
							{{ $tag->alt_title or '-' }}
						</td>
					</tr>
					<tr>
						<td>
							Description
						</td>
						<td>
							{{ $tag->description or '-' }}
						</td>
					</tr>
					<tr>
						<td>
							Type
						</td>
						<td>
							{{ PotatoHelper::getTagGroupKey($tag->type) }}
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<h5 class="text-danger">Contribution Information</h5>
						</td>
					</tr>
					<tr>
						<td>
							Submitted by
						</td>
						<td>
							<a href="">{{ $tag->user->getUserName() }}</a>
						</td>
					</tr>
					<tr>
						<td>
							Submitted on
						</td>
						<td>
							{{ $tag->created_at->format('M d, Y \a\t h:m') }}
						</td>
					</tr>
					<tr>
						<td>
							Last updated
						</td>
						<td>
							{{ $tag->created_at->format('M d, Y \a\t h:m') }}
						</td>
					</tr>
					<tr>
						<td>
							Status
						</td>
						<td>
							{{ $tag->getStatus() }}
						</td>
					</tr>
				</tbody>
			</table>
			@if($tag->status == 10)
				<contribute-moderation :id="{{ $tag->id }}" :type='2' :tags='false'></contribute-moderation>
				@if(Auth::check())
					<contribute-edit-tag :type='2' :tag='{{ json_encode($tag->only('id', 'user_id', 'title', 'j_title', 'alt_title', 'description')) }}'></contribute-edit-tag>
				@endif
			@endif

		</div>
		</div>
	</div>

@endsection
