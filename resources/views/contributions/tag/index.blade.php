@extends('layouts.app')

@section('title', 'Add new Tag')

@section('content')


	<add-tag :category='{!! json_encode(PotatoHelper::$tagGroups) !!}'></add-tag>

@endsection
