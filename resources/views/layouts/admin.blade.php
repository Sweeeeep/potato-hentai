<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>@yield('title') - Admin Panel | {{ config('app.name') }} , we fap and eat potato</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="stylesheet" href="{{ asset('assets/css/admin.min.css') }}">
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
	</head>
	<body class="sidebar-fixed header-fixed">
		<div class="page-wrapper" id="app">
		    <nav class="navbar page-header">
		        <a href="#" class="btn btn-link sidebar-mobile-toggle d-md-none mr-auto">
		            <i class="fa fa-bars"></i>
		        </a>

		        <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
					Potato Hentai Admin Panel
		            {{-- <img src="{{ asset('assets/images/logo4.png')}}" alt="logo"> --}}
		        </a>

		        <a href="#" class="btn btn-link sidebar-toggle d-md-down-none">
		            <i class="fa fa-bars"></i>
		        </a>

		        <ul class="navbar-nav ml-auto">
					<li class="nav-item d-md-down-none">
						<a href="{{ route('index') }}">
							<i class="fa fa-home"></i>
						</a>
					</li>

		            <li class="nav-item d-md-down-none">
		                <a href="#">
		                    <i class="fa fa-bell"></i>
		                    <span class="badge badge-pill badge-danger">5</span>
		                </a>
		            </li>

		            <li class="nav-item d-md-down-none">
		                <a href="#">
		                    <i class="fa fa-envelope-open"></i>
		                    <span class="badge badge-pill badge-danger">5</span>
		                </a>
		            </li>

		            <li class="nav-item dropdown">
		                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                    {{-- <img src="./imgs/avatar-1.png" class="avatar avatar-sm" alt="logo"> --}}
		                    <span class="small ml-1 d-md-down-none">{{ auth()->user()->getUserName() }}</span>
		                </a>

		                <div class="dropdown-menu dropdown-menu-right">
		                    <div class="dropdown-header">Account</div>

		                    <a href="#" class="dropdown-item">
		                        <i class="fa fa-user"></i> Profile
		                    </a>

		                    <a href="#" class="dropdown-item">
		                        <i class="fa fa-envelope"></i> Messages
		                    </a>

		                    <div class="dropdown-header">Settings</div>

		                    <a href="#" class="dropdown-item">
		                        <i class="fa fa-bell"></i> Notifications
		                    </a>

		                    <a href="#" class="dropdown-item">
		                        <i class="fa fa-wrench"></i> Settings
		                    </a>

		                    <a href="#" class="dropdown-item">
		                        <i class="fa fa-lock"></i> Logout
		                    </a>
		                </div>
		            </li>
		        </ul>
		    </nav>

		    <div class="main-container">
		        <div class="sidebar">
		            <nav class="sidebar-nav">
		                <ul class="nav">
		                    <li class="nav-title">Navigation</li>

		                    <li class="nav-item">
		                        <a href="{{ route('admin.dashboard') }}" class="nav-link {{ Route::currentRouteNamed('admin.dashboard') ? 'active' : '' }}">
		                            <i class="icon icon-speedometer"></i> Dashboard
		                        </a>
		                    </li>

		                    <li class="nav-item nav-dropdown">
		                        <a href="#" class="nav-link nav-dropdown-toggle">
		                            <i class="icon icon-people"></i> Users Management <i class="fa fa-caret-left"></i>
		                        </a>

		                        <ul class="nav-dropdown-items">
		                            <li class="nav-item">
		                                <a href="{{ route('admin.user') }}" class="nav-link {{ Route::currentRouteNamed('admin.user') ? 'active' : '' }}">
		                                    <i class="icon icon-user"></i> Users
		                                </a>
		                            </li>

		                            <li class="nav-item">
		                                <a href="{{ route('admin.user.roles') }}" class="nav-link {{ Route::currentRouteNamed('admin.user.roles') ? 'active' : '' }}">
		                                    <i class="icon icon-layers"></i> Roles
		                                </a>
		                            </li>

		                            <li class="nav-item">
		                                <a href="{{ route('admin.user.permissions') }}" class="nav-link {{ Route::currentRouteNamed('admin.user.permissions') ? 'active' : '' }}">
		                                    <i class="icon icon-organization"></i> Permission
		                                </a>
		                            </li>
		                        </ul>
		                    </li>

		                    <li class="nav-item nav-dropdown">
		                        <a href="#" class="nav-link nav-dropdown-toggle">
		                            <i class="icon icon-layers"></i> Gallery Management <i class="fa fa-caret-left"></i>
		                        </a>

		                        <ul class="nav-dropdown-items">
		                            <li class="nav-item">
		                                <a href="{{ route('admin.gallery') }}" class="nav-link {{ Route::currentRouteNamed('admin.gallery') ? 'active' : '' }}">
		                                    <i class="icon icon-list"></i> Galleries
		                                </a>
		                            </li>
									{{-- <li class="nav-item">
		                                <a href="alerts.html" class="nav-link">
		                                    <i class="icon icon-energy"></i> Contributions
		                                </a>
		                            </li> --}}
									{{-- <li class="nav-item">
		                                <a href="alerts.html" class="nav-link">
		                                    <i class="icon icon-trash"></i> Deleted Galleries
		                                </a>
		                            </li> --}}
		                        </ul>
		                    </li>

		                    <li class="nav-item nav-dropdown">
		                        <a href="#" class="nav-link nav-dropdown-toggle">
		                            <i class="icon icon-tag"></i> Tag Management <i class="fa fa-caret-left"></i>
		                        </a>

		                        <ul class="nav-dropdown-items">
		                            <li class="nav-item">
		                                <a href="{{ route('admin.tag') }}" class="nav-link {{ Route::currentRouteNamed('admin.tag') ? 'active' : '' }}">
		                                    <i class="icon icon-grid"></i> Tags
		                                </a>
		                            </li>
		                        </ul>
		                    </li>

		                    {{-- <li class="nav-item">
		                        <a href="forms.html" class="nav-link">
		                            <i class="icon icon-puzzle"></i> Blog
		                        </a>
		                    </li>

		                    <li class="nav-title">More</li> --}}

		                </ul>
		            </nav>
		        </div>

		        <div class="content">
					@yield('content')
		        </div>
		    </div>
		</div>
		<script src="{{ asset('assets/js/app.js') }}"></script>
		<script src="{{ asset('assets/js/vendor.min.js') }}"></script>
		<script src="{{ asset('assets/js/admin.min.js') }}"></script>
		@yield('javascript')
	</body>
</html>
