<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - {{ config('app.name') }} , we fap and eat potato</title>

    <link href="{{ asset('assets/css/vendor.min.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="navbar navbar-expand-lg navbar-dark bg-dark navbar-nopadding">
			<div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
					<img src="{{ asset('assets/images/logo4.png')}}" />
				</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav navbar-main mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/') }}">Browse</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('tag.index') }}">Tags</a>
                        </li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ route('tag.index') }}">
								Contributions
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="{{ route('contribute.upload.index') }}">Upload Gallery</a>
								<a class="dropdown-item" href="{{ route('contribute.tag.index') }}">Add Tag</a>
								<a class="dropdown-item" href="{{ route('contribute') }}">User's Submission</a>
							</div>
						</li>
                    </ul>
                </div>
	        </div>
		</div>
		<div class="navbar navbar-expand-lg navbar-dark bg-primary navbar-user">
			<div class="container">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavDropdown">
					<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
						@guest
							<li class="nav-item">
	                            <a class="nav-link" href="{{ route('login') }}">Sign in</a>
	                        </li>
							<li class="nav-item">
	                            <a class="nav-link" href="{{ route('register') }}">Register</a>
	                        </li>
						@endguest
						@hasrole('admin')
							<li class="nav-item">
								<a class="nav-link" href="{{ route('admin.dashboard') }}">Admin Panel</a>
							</li>
						@endhasrole
						@auth
							<li class="nav-item">
								<a class="nav-link" href="{{ route('user.auth.profile') }}">User Panel</a>
							</li>
						@endauth
					</ul>
					<ul class="navbar-nav my-2 my-lg-0">
						@if(Auth::check())
							<notification :userid="{{ auth()->id() }}"></notification>
							<li class="nav-item dropdown dropdown-hover">
									<a class="nav-link dropdown-toggle" href="JavaScript:void();">
										{{ Auth::user()->getUserName() }}
									</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="{{ route('user.setting') }}">Settings</a>
										<a class="dropdown-item" href="Javascript::void()" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
									</div>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
							</li>
						@endif
					</ul>
				</div>
			</div>
		</div>

        <div class="container">
            @yield('content')
        </div>
    </div>
	<script>
	    window.Laravel = @json([
	        'user' => Auth::user()
	    ])
	</script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
</body>
</html>
