@extends('layouts.app')

@section('title', 'Browse')

@section('content')
	
	{{-- <div class="row" id="cards-padding"> --}}
		<gallery></gallery>
		{{-- @foreach ($galleries as $gallery)
			<div class="card gallery zero-padding text-dark">
				<a href="{{ route('gallery.view', [ 'slug' => $gallery->slug ]) }}">
					<img class="card-img-top" src="{{ PotatoHelper::getCover($gallery->id) }}" alt="{{ $gallery->title }}">
					<div class="title-main">{{ $gallery->title }}<br/>{{ $gallery->j_title }}</div>
				</a>
				<div class="card-footer">
					<div class="meta-head">
						<div class="head-content">
							<div class="pull-right">
								<span class="badge badge-pages">{{ $gallery->page ? $gallery->page->total_pages : '?'}} Pages</span>
							</div>
							<span class="badge badge-text">Touhou Project</span>
						</div>
					</div>
					<div class="meta-body">
						<div class="title"><a href="{{ route('gallery.view', [ 'slug' => $gallery->slug ]) }}">{{ $gallery->title }}<br/>{{ $gallery->j_title }}</a></div>
						@php
							$contentTags = PotatoHelper::sortTagsByGroup($gallery->tags, 4);
						@endphp
						@if(is_array($contentTags))
						<div class="meta-data">
							<label>Contents:</label>
							<ul>
								@foreach ($contentTags as $tags)
									@foreach (array_slice($tags, 0, 2) as $tag)
										<li>
											<a href="">{{ $tag->title }}</a>
										</li>
									@endforeach
								@endforeach
							</ul>
						</div>
					@endif
						@if($gallery->description)
							<div class="meta-data">
								<label>Summary:</label>
								<span>{{ str_limit($gallery->description, 100) }}</span>
							</div>
						@endif
					</div>
				</div>
			</div>
		@endforeach --}}
	{{-- </div> --}}
	{{-- {{ $galleries ->render() }} --}}
@endsection
