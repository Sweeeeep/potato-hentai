@extends('layouts.app')

@section('title', 'Read '.$gallery->title)

@section('content')

	<div class="jumbotron">
		<div class="row">
			<div class="col-md-3 col-sm-12">
				<img style="width:100%;" class="gallery-cover" src="{{ PotatoHelper::getCover($gallery->id) }}" />
			</div>
			<div class="col-md-9 col-sm-12">
				<h4>{{ $gallery->title }} <br /> {{ $gallery->j_title }}</h4>
				<i>
					{{ $gallery->alt_title }}
				</i>

				<p>
					{{ $gallery->description }}
				</p>

				<table class="table table-gallery-info">
					<tbody>
						@if(is_array($sortedTags))
						@foreach ($sortedTags as $group => $tags)
							@if(count($tags))
								<tr>
									<td>{{ $group }}</td>
									<td>
										<ul class="list-inline">
											@foreach ($tags as $value)
												<li>
													<a href="{{ route('tag.view', ['type' => strtolower($group), 'slug' => $value->slug]) }}">{{ $value->title }}</a>
												</li>
											@endforeach
										</ul>
									</td>
								</tr>
							@endif
						@endforeach
					@endif
						<tr>
							<td>
								Pages
							</td>
							<td>
								{{ $gallery->page ? $gallery->page->total_pages : '0' }}
							</td>
						</tr>
						<tr>
							<td>
								Uploader
							</td>
							<td>
								<a href="">{{ $gallery->user->getUserName() }}</a>
							</td>
						</tr>
						<tr>
							<td>
								Rating
							</td>
							<td>
								<gallery-rating :current-rating="{{ $gallery->avgRating->rating or '0' }}" :galleryid="{{ $gallery->id }}"></gallery-rating>
							</td>
						</tr>
					</tbody>
				</table>
				<a href="{{ route('gallery.read', ['slug' => $gallery->slug ]) }}" class="btn btn-outline-danger btn-strong">Read Online</a>
				<a href="#" class="btn btn-outline-primary btn-strong">Download</a>
				<gallery-user-control :gallery='{!! json_encode($gallery->only('id')) !!}'></gallery-user-control>
				<contribute-edit-gallery :gallery="{{ json_encode($gallery->only('id', 'title', 'j_title', 'description')) }}" :tags="{{ json_encode($sortedTags) }}" :language="{{ json_encode($languages) }}" :category="{{ json_encode($category) }}" :type='1'></contribute-edit-gallery>

			</div>
		</div>
		<br />
		@if(isset($collections))
			<h2>Collections</h2>
			<hr class="my-4">
			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Title</th>
						<th scope="col">Pages</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($collections as $key => $collection)
						<tr class="text-dark {{ $collection->id == $gallery->id ? 'table-active' : ''}}"  onclick="window.location='{{ route('gallery.view', [
								'slug' => $collection->slug
							])}}';">
							<th>#{{ $key+1 }}</th>
							<th>{{ $collection->getGalleryName() }}</th>
							<th>{{ $collection->page->total_pages or '0'}}</th>
						</tr>
					@endforeach
				</tbody>
			</table>
		@endif
		<h2>Thumbnails</h2>
		<hr class="my-4">
			@php
				$totalPages = $gallery->page ? $gallery->page->total_pages : 0;
			@endphp
			<gallery-thumbnails total="{{ $totalPages }}" :gallery="{{ json_encode($gallery->only(['id', 'slug'])) }}"></gallery-thumbnails>
	</div>

@endsection
