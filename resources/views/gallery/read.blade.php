@extends('layouts.app')

@section('title', 'Read '.$gallery->title)

@section('content')
	<read-gallery v-bind:gallery='{!! $gallery->toJson() !!}'></read-gallery>
	<router-view></router-view>
@endsection
