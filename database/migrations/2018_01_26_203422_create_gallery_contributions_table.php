<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_contributions', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('gallery_id');
			$table->integer('user_id');
            $table->string('title');
			$table->string('slug');
            $table->string('j_title')->nullable();
			$table->string('alt_title')->nullable();
			$table->text('description')->nullable();
			$table->text('note')->nullable();
			$table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery_contributions');
    }
}
