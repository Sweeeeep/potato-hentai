<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
			$table->string('j_title')->nullable();
            $table->string('alt_title')->nullable();
			$table->string('slug');
			$table->integer('type');
			$table->string('description')->nullable();
            $table->integer('status');
			$table->dateTime('approved_at');
            $table->timestamps();
        });

        Schema::create('gallery_tag', function (Blueprint $table) {
            $table->integer('gallery_id');
            $table->integer('tag_id');
            $table->primary(['gallery_id', 'tag_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
        Schema::dropIfExists('gallery_tag');
    }
}
