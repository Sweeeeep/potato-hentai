<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_contributions', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('user_id');
			$table->integer('tag_id');
            $table->string('title');
			$table->string('j_title')->nullable();
            $table->string('alt_title')->nullable();
			$table->string('slug');
			$table->text('description')->nullable();
            $table->integer('status');
            $table->timestamps();
        });

		Schema::create('tag_gallery_contributions', function(Blueprint $table) {
			$table->integer('tc_id');
			$table->integer('gc_id');
			$table->primary(['tc_id', 'gc_id']);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('tag_contributions');
        Schema::dropIfExists('tag_gallery_contributions');
    }
}
