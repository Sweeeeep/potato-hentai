<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model{

	use Sluggable;
	use SluggableScopeHelpers;

	// Status
	// 1 = Approved
	// 0 = Disapproved
	// 10 = pending

	protected $fillable = ['user_id', 'title', 'j_title', 'alt_title' , 'type' ,'description', 'status'];

	public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	public function gallery(){
		return $this->belongsToMany(Gallery::class);
	}

	public function contriGallery(){
		return $this->belongsToMany(GalleryContribution::class, 'tag_gallery_contributions', 'tc_id', 'gc_id');
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function getTagName(){
		return $this->j_title ? $this->title . ' / '. $this->j_title : $this->title;
	}

	public static function getAll(){

		return self::with(['gallery' => function($q){
			$q->where('status', 1);
		}])->where('status', 1)->orderBy('title', 'ASC')->withCount(['gallery' => function($q){
			$q->where('status', 1);
		}])->orderBy('gallery_count', 'DESC')->paginate(20);
	}


	public function getStatus(){
		if($this->status == 1){
			return 'Approved';
		}elseif ($this->status == 0) {
			return 'Dissaproved';
		}else{
			return 'Pending review';
		}
	}

	public function sortByType(){

	}

	public static function getTagInfo($slug, $sort = null){
		$tag = self::whereSlug($slug)->select('id', 'title', 'slug', 'j_title', 'description', 'alt_title', 'type')->first();

		// $gallery = Gallery::with(['tags' => function($q)use($tag){
		// 	$q->where('id', '=', $tag->id);
		// }, 'page']);

		$gallery = $tag->gallery()->where('status', 1)->with(['tags' => function($q){
			$q->select('id', 'title', 'type')->where('type', 2)->orWhere('type', 7);
		}, 'page']);

		if($sort == 'newest'){
			$gallery = $tag->orderBy('created_at', 'DESC');
		}elseif($sort == 'most-viewed'){
			$gallery = $tag->orderBy('views', 'DESC');
		}elseif($sort == 'title'){
			$gallery = $tag->orderBy('title', 'ASC');
		}elseif($sort == 'random'){
			$gallery = $tag->inRandomOrder();
		}

		$gallery = $gallery->paginate(20);
		return compact('tag', 'gallery');
	}

	public static function sortTagByType($type){
		$tag = self::where('type', $type)->withCount(['gallery' => function($q){
			$q->where('status', 1);
		}])->orderBy('gallery_count', 'DESC');

		$tags = $tag->paginate(20);

		return $tags;
	}

	public static function search($type, $query){
		$tags = self::where('type', $type)->where('status', '!=', 0)->where('title', 'LIKE', '%'.$query);

		if(empty($query)){
			$tags = $tags->limit('10');
		}

		$tags = $tags->select('id', 'title')->get();

		return $tags;
	}


}
