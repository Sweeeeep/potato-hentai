<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class TagContribution extends Model{

	use Sluggable;
	use SluggableScopeHelpers;

	protected $fillable = ['user_id', 'tag_id', 'title', 'j_title', 'alt_title', 'description', 'status'];

	public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	public function getTagName(){
		return $this->j_title ? $this->title . ' / '. $this->j_title : $this->title;
	}

	public function getStatus(){
		if($this->status == 1){
			return 'Approved';
		}elseif ($this->status == 0) {
			return 'Dissaproved';
		}else{
			return 'Pending review';
		}
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function tag(){
		return $this->belongsTo(Tag::class);
	}

	public function contriGallery(){
		return $this->belongsToMany(GalleryContribution::class, 'tag_gallery_contributions', 'tc_id', 'gc_id');
	}
}
