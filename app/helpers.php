<?php

class PotatoHelper{

	public static $tagGroups = [
		'Artist', 'Character', 'Contents', 'Collection', 'Circle', 'Category', 'Language', 'Parody'
	];

	public static function sortTagsByGroup($array, $group = null, $callback = null){

		foreach (self::$tagGroups as $type => $name) {
			$callback[$name] = [];
			$array = $array->sortBy('type');
			foreach ($array as $tag) {
				if($group && $type == $group && $group = $tag->type){
					$callback[$name][] = $tag;
				}
				if($type == $tag->type){
					$callback[$name][] = $tag;
				}
			}
		}
		return $callback;

	}

	public static function getTagGroupValue($name){
		foreach (self::$tagGroups as $key => $value) {
			if(ucfirst($name) == ucfirst($value)){
				return $key;
			}
		}
	}

	public static function getTagGroupKey($tkey){
		foreach (self::$tagGroups as $key => $value) {
			if($key == $tkey){
				return $value;
			}
		}
	}

	public static function getTagID($arr, $res = array()){
		foreach ($arr as $key => $value) {
			$res[] = $value->id;
		}
		return $res;
	}

	public static function checkTagStatus($arr){
		foreach ($arr as $key => $value) {
			if($value->status == 10){
				return true;
			}
		}
		return false;
	}

	public static function getTagType($array, $check, $results = array()){
		foreach (self::$tagGroups as $type => $name) {
			foreach ($array as $tag) {
				if(!in_array($tag->id, $check)){
					$make = true;
					if($type == $tag->type){
						foreach ($results as $key => $value) {
							if($value['type'] == $name){
								array_push($results[$key]['items'], $tag);
								$make = false;
							}else{
								$make = true;
							}
						}
						if($make){
							$results[] = [
								'type' => $name,
								'items' => array(
									$tag
								)
							];
						}
					}
				}
			}
		}
		return $results;
	}


	public static function getCover($id){
		$path = 'assets/images/gallery/';
		$cover = Storage::disk('gallery')->exists($path.$id.'/cover.jpg');
		if($cover){
			return asset($path.$id.'/cover.jpg');
		}else{
			return asset($path.'/no-cover.jpg');
		}
	}

	public static function getUserCollection($galleries){
		$array = [];
		// $check = Tag::whereIn('id', $array)->first();
		// dd($check->pivot);

		foreach ($galleries as $key => $value) {
			$array[] = $value->id;
		}

		return App\Http\Controllers\TagController::getWBUser($array);
	}

	public static function filterCard($data){
		arsort($data);
		$class = ''; $ribbonName = ''; $ribbonClass = '';
		foreach ($data as $key => $value) {
			if($value == 2){
				$class = 'card-blacklisted';
				$ribbonName = 'Blacklisted';
				$ribbonClass = 'gray';
				break;
			}elseif($value == 3){
				$class = 'card-favorite';
				$ribbonName = 'Favorite';
				$ribbonClass = 'blue';
				break;
			}elseif ($value == 4) {
				$class = 'card-bookmark';
				$ribbonName = 'Bookmark';
				$ribbonClass = 'green';
				break;
			}elseif($value == 1){
				$class = 'card-like';
				$ribbonName = 'Liked';
				$ribbonClass = 'red';
				break;
			}
		}
		return [
			'class' => $class,
			'ribbonName' => $ribbonName,
			'ribbonClass' => $ribbonClass
		];
		// return compact($class, $ribbonName, $ribbonClass);
	}

	public static function maskEmail($email, $show=3) {
		return substr($email, 0, $show).'****'.substr($email, strpos($email, "@"));;
	}

}


class PotatoFileHandler{

	public static function getFileExtension($fileName){
		return pathinfo($fileName, PATHINFO_EXTENSION);
	}

	public static function getFileInfo($file){
		return pathinfo($file);
	}

	public static function extract($file, $path){

		$fileName = $file['name'].$file['extension'];
		$save_path =  public_path($path).'/';

		try{
			$fileUploaded = File::move(storage_path("app/upload/temp/".$fileName), $save_path.$fileName);
		}catch(Exception $e){
			$fileUploaded = Storage::disk('gallery')->exists($path.'/'.$fileName);
			// dd($fileUploaded);
			return $fileUploaded;
		}

		if($fileUploaded){
			if($file['extension'] == '.zip'){
				$zip = new ZipArchive;
				if($zip->open($save_path.$fileName) == TRUE){
					$zip->extractTo($save_path);
					$zip->close();
					return true;
				}else{
					return false;
				}
			}
			// elseif($file['extension'] == '.rar'){
			// 	// $rar = new RarArchive;
			// 	// if($rar->open($save_path.$fileName) == TRUE){
			// 	// 	$entries = $archive->getEntries();
			// 	// 	foreach ($entries as $entry) {
			// 	// 		$entry->extract($save_path);
			// 	// 	}
			// 	// 	$archive->close();
			// 	// 	return true;
			// 	// }else{
			// 	// 	return false;
			// 	// }
			// 	$rar = rar_open($save)
			// }
			return true;
		}
	}

	public static function getImages($path, $results = array()){
		$images = Storage::disk('gallery')->files($path);
		foreach($images as $image){
			if(self::isImage($image)){
				$results[] = $image;
			}else{
			}
		}
		return $results;
	}

	public static function checkZip($path, $result = array()){

		$logFiles = Zipper::make($path)->listFiles('/^(?!.*\.jpg|.png).*$/i');
		if(count($logFiles)){
			return false;
		}else{
			return true;
		}


		$zip = new ZipArchive;
		if($zip->open($path) == TRUE){
			echo $zip->numFiles;
			for ($i = 0; $i < $zip->numFiles; $i++) {
				$check = $zip->statIndex($i);
				$result[] = $check;
			}
		}

		return $result;

	}

	public static function isImage($image){
		$extension = self::getFileExtension($image);
		$extensionAccepts = ['jpg', 'png'];
		if(in_array($extension, $extensionAccepts)){
			return true;
		}
		return false;

	}

	public function extractRar($file, $path){
		if($file['extension'] == 'zip'){
			$fileName = $file['name'].$file['extension'];
			$save_path =  public_path($path).'/';
			$fileUploaded = Storage::move(storage_path("app/upload/temp/".$fileName), $save_path);

			if($fileUploaded){
				$rar = new RarArchive;
				if($rar->open($save_path.$fileName) == TRUE){
					$entries = $archive->getEntries();
					foreach ($entries as $entry) {
						$entry->extract($save_path);
					}
					$archive->close();
					return true;
				}else{
					return false;
				}
			}
			return false;
		}
	}

}
