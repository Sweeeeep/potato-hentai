<?php

// Status
// 1 = Approved
// 0 = Disapproved
// 8 = deleted
// 9 = invalid zip
// 10 = pending

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use PotatoHelper;
use Cache;

class Gallery extends Model{


	use Sluggable;
	use SluggableScopeHelpers;

	protected $fillable = ['title', 'j_title', 'alt_title', 'description', 'source'];

	public function __construct(){
		parent::__construct();
	}

	public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	public function tags(){
		return $this->belongsToMany(Tag::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function page(){
		return $this->hasOne(Page::class);
	}

	public function rating(){
		return $this->hasMany(Rating::class);
	}

	public function avgRating(){
    	return $this->rating()->selectRaw('avg(rating) as rating, gallery_id')->groupBy('gallery_id');
	}

	public function getAvgRatingAttribute(){
	    if ( ! array_key_exists('avgRating', $this->relations)) {
	       $this->load('avgRating');
	    }
	    $relation = $this->getRelation('avgRating')->first();
	    return ($relation) ? $relation->aggregate : null;
	}

	public function getGalleryName(){
		return $this->j_title ? $this->title . ' / '. $this->j_title : $this->title;
	}

	public function getStatus(){
		if($this->status == 1){
			return 'Approved';
		}elseif ($this->status == 0) {
			return 'Dissaproved';
		}elseif ($this->status == 9) {
			return 'Invalid Zip';
		}else{
			return 'Pending review';
		}
	}

	public function formatCreatedAt(){
		return $this->created_at->format('M d, Y \a\t h:i');
	}



	public static function galleries(){
		return self::orderBy('created_at', 'DESC')->where('status', '1')->with('tags', 'page')->paginate(20);
	}

	public static function view($slug){

		if(!Cache::has('GALLERY_VIEW_'.$slug)){
			$gallery = self::whereSlug($slug)->where('status', 1)->with(['tags' => function($q){
				$q->select('id', 'title', 'slug', 'type', 'status');
			},'page' => function($q){
				$q->select('gallery_id', 'total_pages');
			},'avgRating', 'user' => function($q){
				$q->select('id', 'name');
			}])->firstOrFail();
			$sortedTags = PotatoHelper::sortTagsByGroup($gallery->tags);
			$category = Tag::search(5, '');
			$languages = Tag::search(6, '');

			$result = array( 'gallery' => $gallery, 'sortedTags' => $sortedTags, 'category' => $category, 'languages' => $languages);

			if(isset($sortedTags['Collection']) && count($sortedTags['Collection'])){
				$collection = $sortedTags['Collection'][0];

				$collections = Gallery::where('status', 1)->whereIn('id', function($query)use($collection){
					$query->select('gallery_id')->from('gallery_tag')->where('tag_id', $collection->id)->groupBy('gallery_id')->havingRaw('COUNT(*) = 1');
				})->with(['page' => function($q){
					$q->select('gallery_id', 'total_pages');
				}])->orderBy('title', 'ASC')->select('id', 'title', 'j_title', 'slug')->get();

				$result['collections'] = $collections;

			}





			Cache::put('GALLERY_VIEW_'.$slug, $result , 1000);

			return $result;
		}
		return Cache::get('GALLERY_VIEW_'.$slug);
	}

	public static function getBySlug($slug){
		return self::whereSlug($slug)->with('page')->first();
	}

}
