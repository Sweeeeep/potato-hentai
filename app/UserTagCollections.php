<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTagCollections extends Model{

	protected $fillable = ['tag_id', 'user_id', 'type'];

	public function tags(){
		return $this->belongsTo(Tag::class, 'tag_id', 'id');
	}

}
