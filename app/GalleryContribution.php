<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;

class GalleryContribution extends Model
{

	use Sluggable;
	use SluggableScopeHelpers;

	protected $fillable = ['user_id', 'gallery_id', 'title', 'slug', 'j_title', 'alt_title', 'description', 'source', 'status'];

	public function sluggable(){
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function tags(){
		return $this->belongsToMany(Tag::class, 'tag_gallery_contributions', 'gc_id', 'tc_id');
	}

	public function gallery(){
		return $this->belongsTo(Gallery::class);
	}

	public function getGalleryName(){
		return $this->j_title ? $this->title . ' / '. $this->j_title : $this->title;
	}

	public function getStatus(){
		if($this->status == 1){
			return 'Approved';
		}elseif ($this->status == 0) {
			return 'Dissaproved';
		}elseif ($this->status == 9) {
			return 'Invalid Zip';
		}else{
			return 'Pending review';
		}
	}
}
