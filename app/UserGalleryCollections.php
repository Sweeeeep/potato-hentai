<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGalleryCollections extends Model
{
    protected $fillable = ['gallery_id', 'user_id', 'type'];

	public function gallery(){
		return $this->belongsTo(Gallery::class, 'gallery_id', 'id');
	}
}
