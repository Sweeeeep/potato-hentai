<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model{

    protected $fillable = ['total_pages'];

	public function gallery(){
		return $this->belongsTo(Gallery::class);
	}
}
