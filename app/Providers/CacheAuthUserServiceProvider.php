<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CacheAuthUserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
		$this->app['auth']->provider('cacheableEloquent',
            function($app, $config) {
                $config['model']::updated(function ($model) {
                    CacheAuthUserProvider::clearCache($model);
                });
                return new CacheAuthUserProvider($app['hash'], $config['model']);
            }
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
