<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
	use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


	public function scopeGetUserName(){
		if(is_null($this->name) || $this->name == ''){
			return 'Anonymous'.$this->id;
		}
		return $this->name;
	}

	public function gallery(){
		return $this->hasMany(Gallery::class);
	}

	public function contriGallery(){
		return $this->hasMany(GalleryContribution::class);
	}

	public function tags(){
		return $this->hasMany(User::class);
	}

	public static function getInfo($id){
		return self::where('id', $id)->first();
	}
}
