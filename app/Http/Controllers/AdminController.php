<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Gallery;
use App\Tag;
use App\User;

use App\GalleryContribution;
use App\TagContribution;

use Carbon\Carbon;
use PotatoHelper;

class AdminController extends Controller{

	public function dashboard(){
		$gallery = Gallery::count();
		$tag = Tag::count();
		$user = User::count();

		$chart = User::orderBy('created_at', 'ASC')->whereYear('created_at', Carbon::now()->year)->get(['id', 'created_at'])->groupBy(function($date){
			return Carbon::parse($date->created_at)->format('m');
		});
		$chartData = [];

		$gc = GalleryContribution::count();
		$tc = TagContribution::count();

		$contribute = $gc + $tc;

		foreach ($chart as $key => $value) {
			$chartData[] = count($value);
		}

		return view('admin.dashboard', compact('gallery', 'tag', 'user', 'contribute', 'chartData'));
	}

	public function users(Request $request){
		$users = User::query();
		if($request->search_query){
			$users = $users->orWhere('name', 'LIKE', '%'.$request->search_query.'%')->orWhere('username', 'LIKE', '%'.$request->search_query.'%')->orWhere('email', 'LIKE', '%'.$request->search_query.'%');
		}
		$users = $users->paginate(20);

		return view('admin.users.index', compact('users'));
	}

	public function userUpdate($id){
		$user = User::where('id', $id)->firstOrFail();

		$roles = Role::all();

		return view('admin.users.update', compact('user', 'roles'));
	}

	public function userUpdatePost($id, Request $request){

		$user = User::where('id', $id)->firstOrFail();

		if($user->name != $request->display_name){
			$request->validate([
				'display_name' => 'unique:users,name'
			]);
		}

		if(!empty($request->password)){
			$user->password = Hash::make($request->password);
		}
		$user->name = $request->display_name;
		$user->save();

		$user->syncRoles($request->role);

		flash('Successfully Update user.')->success();

		return back();
	}

	public function roles(){

		$roles = Role::all();

		return view('admin.users.roles.index', compact('roles'));

	}

	public function roleUpdate($id){
		$role = Role::where('id', $id)->firstOrFail();

		$permissions = Permission::all();

		return view('admin.users.roles.update', compact('role', 'permissions'));
	}

	public function roleUpdatePost($id, Request $request){
		$role = Role::where('id', $id)->firstOrFail();

		$role->name = $request->name;

		$role->syncPermissions($request->permissions);

		flash('Successfully Update role.')->success();

		return back();

	}

	public function roleCreate(){
		$permissions = Permission::all();

		return view('admin.users.roles.create', compact('permissions'));
	}

	public function roleCreatePost(Request $request){
		$role = Role::create([
			'guard_name' => 'web',
			'name' => $request->name
		]);

		$role->syncPermissions($request->permissions);

		flash('Successfully created role.')->success();

		return back();

	}

	public function permissions(){
		$permissions = Permission::all();

		return view('admin.users.permissions.index', compact('permissions'));
	}

	public function permissionUpdate($id){
		$permission = Permission::where('id', $id)->firstOrFail();

		return view('admin.users.permissions.update', compact('permission'));
	}

	public function permissionUpdatePost($id, Request $request){
		$permission = Permission::where('id', $id)->firstOrFail();

		$permission->name = $request->name;

		flash('Successfully Update permission.')->success();

		return back();

	}


	// galleries section

	public function galleries(){
		$galleries = Gallery::with(['user' => function($q){
			$q->select('id', 'name');
		}])->paginate(20);

		return view('admin.gallery.index', compact('galleries'));
	}

	public function updateGallery($id){
		$gallery = Gallery::where('id', $id)->with('tags')->first();

		$sortedTags = PotatoHelper::sortTagsByGroup($gallery->tags);

		$category = Tag::search(5, '');
		$languages = Tag::search(6, '');


		return view('admin.gallery.update', compact('gallery', 'sortedTags', 'category', 'languages'));
	}

	public function updateGalleryPost(Request $request){
		$gallery = Gallery::where('id', $request->id)->where('status', 10)->first();
		if($gallery){
			$gallery->title = $request->title;
			$gallery->j_title = $request->j_title;
			$gallery->alt_title = $request->alt_title;
			$gallery->description = $request->description;
			$tags = [
				$request->Category, $request->Language
			];
			foreach ($request->tags as $group => $type) {
				foreach ($type as $key => $tag) {
					if($tag['id'] == 0){
						$title = str_replace('(new)', '', $tag['title']);
						$tag = Tag::firstOrCreate([
							'title' => trim($title),
							'type' => PotatoHelper::getTagGroupValue($group),
							'user_id' => $gallery->user_id,
							'status' => 1
						]);
						$tags[] = $tag->id;
					}else{
						$tags[] = $tag['id'];
					}
				}
			}
			$gallery->tags()->sync($tags);
			$gallery->save();

			return response()->json([
				'status' => true,
				'message' => 'Successfully updated!'
			]);
		}else{
			return response()->json([
				'status' => false,
				'message' => 'Invalid GID'
			]);
		}

	}

	public function deleteGallery($id){
		$gallery = Gallery::where('id', $id)->update([
			'status' => 8
		]);

		flash('Successfully deleted temporary.')->success();

		return back();
	}

	public function recoverGallery($id){
		$gallery = Gallery::where('id', $id)->update([
			'status' => 1
		]);

		flash('Successfully recover.')->success();

		return back();
	}

	// TAGS

	public function tags(){
		$tags = Tag::with(['user' => function($q){
			$q->select('id', 'name');
		}])->paginate(20);

		return view('admin.tag.index', compact('tags'));
	}

	public function updateTag($id){
		$tag = Tag::where('id', $id)->firstOrFail();

		return view('admin.tag.update', compact('tag'));
	}

	public function updateTagPost($id, Request $request){

		$request->validate([
			'title' => 'required',
			'type' => 'required'
		]);

		$tag = Tag::where('id', $id)->update([
			'title' => $request->title,
			'j_title' => $request->j_title,
			'alt_title' => $request->alt_title,
			'type' => $request->type,
			'description' => $request->description,
		]);

		flash('Successfully updated.')->success();

		return back();
	}

	public function deleteTag($id){
		$gallery = Tag::where('id', $id)->update([
			'status' => 8
		]);

		flash('Successfully deleted temporary.')->success();

		return back();
	}

	public function recoverTag($id){
		$gallery = Tag::where('id', $id)->update([
			'status' => 1
		]);

		flash('Successfully recover.')->success();

		return back();
	}

}
