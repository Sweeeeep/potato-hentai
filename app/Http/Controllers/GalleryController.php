<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;

use PotatoHelper;
use App\Gallery;

use DB;
use Cache;
use Image;
use Storage;


class GalleryController extends Controller{

	public function view($slug){
		$gallery = Gallery::view($slug);
		// dd($gallery);

		return view('gallery.view', $gallery);
	}

	public function read($slug, $page = 0){
		$gallery = Gallery::getBySlug($slug);
		$gallery->page_number = $page;
		return view('gallery.read', compact('gallery'));
	}

	public function images(Request $request){

	}

	public function apiGetGalleries(Request $request){

		$cacheName = ':GALLERIES_PAGE_'.$request->page;
		$cacheKey = md5('GalleryMainBrowse');

		$gallery = Cache::tags($cacheKey)->rememberForever($cacheName, function() {
			$gallery = Gallery::with(['tags' => function($query){
				$query->where('type', 2)->orWhere('type', 7);
			}, 'page', 'avgRating']);

			$gallery = $gallery->orderBy('id', 'DESC')->where('status', 1);

			return $gallery->paginate(20);
		});

		return $gallery;

	}

	public function apiSearchGallery(Request $request){
		$search = $request->search;
		$currentPage = $request->p;

		Paginator::currentPageResolver(function () use ($currentPage) {
	        return $currentPage;
	    });


		$include = array_pluck($search['tags']['include'], 'id');
		$exclude = array_pluck($search['tags']['exclude'], 'id');

		$gallery = Gallery::with(['tags' => function($query){
			$query->where('type', 2)->orWhere('type', 7);
		}, 'page', 'avgRating'])->where('status', 1);


		if(count($include) != 0){
			$gallery = $gallery->whereIn('id', function($query)use($include){
				$query->select('gallery_id')->from('gallery_tag')->whereIn('tag_id', $include)->groupBy('gallery_id')->havingRaw('COUNT(*) = ' . count($include));
			});
		}

		if(count($exclude) != 0){

			$gallery = $gallery->whereNotIn('id', function($query)use($exclude){
				$query->select('gallery_id')->from('gallery_tag')->whereIn('tag_id', $exclude)->groupBy('gallery_id');
			});
		}
		if($search['string'] != null && $search != ''){

			$gallery = $gallery->where('title', 'LIKE', '%'.$search['string'].'%');
			$gallery = $gallery->orwhere('j_title', 'LIKE', 'N%'.$search['string'].'%');
		}

		if($search['sort'] == 2){
			$gallery = $gallery->orderBy('created_at', 'DESC');
		}elseif($search['sort'] == 3){
			$gallery = $gallery->orderBy('title', 'ASC');
		}elseif($search['sort'] == 4){
			$gallery = $gallery->orderBy('views', 'DESC');
		}else{
			$gallery = $gallery->orderBy('id', 'ASC');
		}


		// return $gallery->toSql();

		return $gallery->paginate(20);

		// $gallery = Gallery::with(['tags' => function($query) use($include, $exclude){
		// 	$query->where('type', 4)->orWhere('type', 7)->whereIn('id', $include)->whereNotIn('id', $exclude);
		// }, 'page'])->paginate(20);
        //
		// return $gallery;

	}

	public function getCover($id){
		$path = 'assets/images/gallery/';
		$storage = Storage::disk('gallery')->exists($path.$id.'/cover.jpg');
		if($storage){
			$coverPath = asset($path.$id.'/cover.jpg');
		}else{
			$coverPath = asset($path.'/no-cover.jpg');
		}
        return Image::make($coverPath)->response();
	}

}
