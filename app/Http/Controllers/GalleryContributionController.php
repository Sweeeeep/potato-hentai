<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\GalleryContribution;
use App\Tag;

use PotatoHelper;

class GalleryContributionController extends Controller{


	public function requestEditGallery(Request $request){

		$check = GalleryContribution::where('user_id', auth()->id())->where('gallery_id', $request->id)->where('status', 10)->first();
		if($check){
			return response()->json([
				'status' => false,
				'message' => 'You already request edit for this gallery'
			]);
		}

		$gallery = GalleryContribution::create([
			'user_id' => auth()->id(),
			'gallery_id' => $request->id,
			'title' => $request->title,
			'j_title' => $request->j_title,
			'alt_title' => $request->alt_title,
			'description' => $request->description,
			'status' => 10
		]);

		$tagIDS = [
			$request->Category, $request->Language
		];

		foreach ($request->tags as $group => $tags) {
			foreach ($tags as $key => $tag) {
				if($tag['id'] == 0){
					$title = str_replace('(new)', '', $tag['title']);
					$tag = Tag::firstOrCreate([
						'title' => trim($title),
						'type' => PotatoHelper::getTagGroupValue($group),
						'user_id' => $gallery->user_id,
						'status' => 10
					]);
					$tagIDS[] = $tag->id;
				}else{
					$tagIDS[] = $tag['id'];
				}
			}
		}
		$gallery->tags()->sync($tagIDS);

		return response()->json([
			'status' => true,
			'message' => 'You have successfully submitted your request, Thank you!'
		]);

	}

	public function galleryRequestEditView($slug){

		$gallery = GalleryContribution::whereSlug($slug)->with(['tags' => function($q){
			$q->select('id', 'slug', 'title', 'type', 'status');
		}, 'gallery' => function($q){
			$q->with(['tags' => function($a){
				$a->select('id', 'slug', 'title', 'type', 'status');
			}]);
		}, 'user' => function($q){
			$q->select('id', 'name');
		}])->first();

		$category = Tag::search(5, '');
		$languages = Tag::search(6, '');

		// return $gallery;

		return view('contributions.edit-gallery.view', compact('gallery', 'category', 'languages'));
	}

	public function requestEditModerateGallery(Request $request){
		$check = GalleryContribution::where('id', $request->id)->where('status', 10)->first();

		if($check){
			if($check->user_id == auth()->id()){
				$check->title = $request->title;
				$check->j_title = $request->j_title;
				$check->alt_title = $request->alt_title;
				$check->description = $request->description;
				$check->save();

				$tagIDS = [
					$request->Category, $request->Language
				];

				foreach ($request->tags as $group => $tags) {
					foreach ($tags as $key => $tag) {
						if($tag['id'] == 0){
							$title = str_replace('(new)', '', $tag['title']);
							$tag = Tag::firstOrCreate([
								'title' => trim($title),
								'type' => PotatoHelper::getTagGroupValue($group),
								'user_id' => $gallery->user_id,
								'status' => 10
							]);
							$tagIDS[] = $tag->id;
						}else{
							$tagIDS[] = $tag['id'];
						}
					}
				}

				$check->tags()->sync($tagIDS);

				return response()->json([
					'status' => true,
					'message' => 'You have successfully updated your request'
				]);

			}
		}else{
			return response()->json([
				'status' => false,
				'message' => 'Invalid GEID'
			]);
		}
	}

}
