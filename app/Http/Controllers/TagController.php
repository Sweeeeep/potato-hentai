<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;
use Cache;
use PotatoHelper;

use App\Tag;
use App\UserTagCollections;
use App\UserGalleryCollections;

class TagController extends Controller{

	public function index(){
		$tags = Tag::getAll();

		return view('tags.index', compact('tags'));
	}

	public function view($type, $slug, $sort = null){
		$tag = Tag::getTagInfo($slug, $sort);

		$result = array('tag' => $tag['tag'], 'galleries' => $tag['gallery'], 'type' => $type);

		return view('tags.view', $result);
		return $result;
	}

	public function sort($type, $sort = null){
		$type = PotatoHelper::getTagGroupValue($type);
		$tags = Tag::sortTagByType($type);

		return view('tags.index', compact('tags', 'type'));
	}

	public function search($type, Request $request){
		$type = PotatoHelper::getTagGroupValue($type);
		$results = Tag::search($type, $request->input('q'));


		if(count($results) == 0 && ($request->input('q') != null)){
			$results[] = [
				'id' => 0,
				'title' => $request->input('q') .' (new)'
			];
		}

		return $results;
	}

	public function advanceSearchTag(Request $request, $results = array()){

		// return $request->c;

		$search = Tag::where('title', 'LIKE', '%'.$request->q)->select('id', 'title', 'type')->get();

		$type = PotatoHelper::getTagType($search, $request->c);

		return $type;
		$group = $search->map(function($item, $key){

				// $results[$type]['tags'] = $item;
				$results[] = [
					'type' => $type,
					'items' => array(
						$item
					)
				];
				// return $results;
				return [
					'type' => $type,
					'items' => [
						$item
					]
				];
		});

		return $group;
	}

	public static function getWBUser($galleries){
		if(Auth::guest()){
			return 0;
		}

		$cacheKey = 'USER_'.auth()->id().'_BORDERS';
		$cacheName = ':'.md5(implode('-', $galleries));
		if(Cache::tags($cacheKey)->has($cacheName)){
			$res = Cache::tags($cacheKey)->get($cacheName);
		}else{

			$check = DB::table('gallery_tag')->whereIn('gallery_id', $galleries)->get();

			$tagIDS = [];

			foreach ($check as $key => $value) {
				if(!in_array($value->tag_id, $tagIDS))
				$tagIDS[] = $value->tag_id;
			}

			$tags = UserTagCollections::where('user_id', auth()->id())->whereIn('tag_id', $tagIDS)->get(['tag_id', 'type']);


			$res = [];

			foreach ($check as $key => $gallery) {
				foreach ($tags as $key => $tag) {
					if($gallery->tag_id == $tag->tag_id){
						$res[$gallery->gallery_id][] = $tag->type;
					}
				}
			}

			$userGallery = UserGalleryCollections::where('user_id', auth()->id())->whereIn('gallery_id', $galleries)->get(['gallery_id', 'type']);

			foreach ($userGallery as $key => $ugalery) {
				if(isset($res[$ugalery->gallery_id])){
					$res[$ugalery->gallery_id][] = $ugalery->type;
				}else{
					$res[$ugalery->gallery_id][] = $ugalery->type;
				}
			}

			Cache::tags($cacheKey)->forever($cacheName, $res);

		}


		return $res;
	}

	public function ajaxGetGalleryRibbon(Request $request){

		return self::getWBUser($request->galleries);

	}

	public function test(){
		if(Auth::guest())
			return 0;
	}

}
