<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Hash;
use Cache;

use App\Tag;
use App\User;
use App\Gallery;
use App\UserTagCollections;
use App\UserGalleryCollections;

class UserController extends Controller{

	public function getUserTagCollection(Request $request){

		$check = UserTagCollections::where('tag_id', $request->tag)->where('user_id', auth()->id())->first(['type']);

		return $check;
	}
	public function userTagCollection(Request $request){

		if(auth()->guest()){
			return response()->json([
				'status' => false
			]);
		}

		if(isset($request->type, $request->tag)){
			$tag = UserTagCollections::firstOrCreate([
				'tag_id' => $request->tag,
				'user_id' => auth()->id()
			]);
			if($request->type == $tag->type){
				$tag->type = 0;
			}else{
				$tag->type = $request->type;
			}
			$tag->save();
			Cache::tags('USER_'.auth()->id().'_BORDERS')->flush();
			return response()->json([
				'status' => true
			]);
		}

	}

	public function getUserGalleryCollection(Request $request){
		$check = UserGalleryCollections::where('gallery_id', $request->gallery)->where('user_id', auth()->id())->get(['type']);
		return $check;
	}


	public function userGalleryCollection(Request $request){

		if(auth()->guest()){
			return response()->json([
				'status' => false,
				'message' => 'Please login first'
			]);
		}

		if(isset($request->type , $request->gallery)){

			$gallery = UserGalleryCollections::where('gallery_id', $request->gallery)->where('user_id', auth()->id())->where('type', $request->type)->first();

			if($gallery){
				$gallery->delete();
				$message = 'remove';
			}else{
				UserGalleryCollections::create([
					'gallery_id' => $request->gallery,
					'user_id' => auth()->id(),
					'type' => $request->type
				]);
				$message = 'added';
			}
			$cacheKey = md5('USER_'.auth()->id().'_COLLECTIONS');
			$type = $request->type == 3 ? 'favorites' : 'bookmarks';
			Cache::tags($cacheKey)->forget(':'.$type);
			Cache::tags('USER_'.auth()->id().'_BORDERS')->flush();
			return response()->json([
				'status' => true,
				'message' => 'Successfully '.$message.' to '.($request->type == 3 ? 'Favorite' : 'Bookmark').' list'
			]);

		}
	}

	public function changePassword(Request $request){
		if(auth()->user()){
			$request->validate([
			    'current_password' => 'required',
			    'password' => 'required|confirmed',
			]);

			$user = Auth::user();
			if(Hash::check($request->current_password, $user->password)){
				$user->password = Hash::make($request->password);
				$user->save();
				flash('Successfully change password.')->success();
			}else{
				flash('Invalid current passsword.')->error();
			}

			return back();
		}
	}

	public function changeEmail(Request $request){
		$request->validate([
			'current_email' => 'required|email',
			'email' => 'required|confirmed|unique:users,email'
		]);

		$user = Auth::user();
		if($request->current_email == $user->email){
			$user->email = $request->email;
			$user->save();
			flash('Successfully change email.')->success();
		}else{
			flash('Invalid current email.')->error();
		}

		return back();
	}

	public function setName(Request $request){
		$request->validate([
			'name' => 'required|unique:users,name'
		]);

		$user = Auth();
		if(!empty($user->name)){
			$user->name = $request->name;
			$user->save();
			flash('Successfully set your name.')->success();
		}else{
			flash('You already set your name, Sorry.')->error();
		}
		return back();
	}

	public function authUserProfile(){
		$user = Auth::user();
		$recentUpload = Gallery::select('id', 'title', 'j_title', 'slug')->where('user_id', $user->id)->orderBy('created_at', 'desc')->where('status', 1)->limit(5)->get();

		$tagCollections = UserTagCollections::where('user_id', $user->id)->with(['tags' => function($q){
			$q->select('id', 'title', 'slug', 'type');
		}])->orderBy('type', 'ASC')->select('tag_id', 'type')->get();

		$tagCollections = $tagCollections->groupBy('type');

		$bookmarks = UserGalleryCollections::where('user_id', $user->id)->with(['gallery' => function($q){
			$q->select('id', 'title', 'j_title', 'slug')->where('status', 1);
		}])->where('type', 4)->select('gallery_id', 'type')->orderBy('created_at', 'DESC')->limit(6)->get();

		$favorites = UserGalleryCollections::where('user_id', $user->id)->with(['gallery' => function($q){
			$q->select('id', 'title', 'j_title', 'slug')->where('status', 1);
		}])->where('type', 3)->select('gallery_id', 'type')->orderBy('created_at', 'DESC')->limit(6)->get();

		// dd($galleryCollection);
		return view('user.profile.auth', compact('user', 'recentUpload', 'tagCollections', 'bookmarks', 'favorites'));
	}

	public function authUploads(){
		$cacheKey = md5('USER_'.auth()->id().'_COLLECTIONS');
		if(Cache::tags($cacheKey)->has(':uploads')){
			$uploads = Cache::tags($cacheKey)->get(':uploads');
		}else{
			$uploads = Gallery::select('id', 'title', 'j_title', 'slug', 'created_at')->where('user_id', auth()->id())->orderBy('created_at', 'desc')->where('status', 1)->paginate(20);
			Cache::tags($cacheKey)->forever(':uploads', $uploads);
		}
		return view('user.profile.upload', compact('uploads'));
	}

	public function authFavorites(){
		$cacheKey = md5('USER_'.auth()->id().'_COLLECTIONS');
		if(Cache::tags($cacheKey)->has(':favorites')){
			$favorites = Cache::tags($cacheKey)->get(':favorites');
		}else{
			$favorites = UserGalleryCollections::where('user_id', auth()->id())->with(['gallery' => function($q){
				$q->select('id', 'title', 'j_title', 'slug')->where('status', 1);
			}])->where('type', 3)->select('gallery_id', 'type', 'created_at')->orderBy('created_at', 'DESC')->paginate(30);
			Cache::tags($cacheKey)->forever(':favorites', $favorites);
		}
		return view('user.profile.favorite', compact('favorites'));
	}

	public function authBookmark(){
		$cacheKey = md5('USER_'.auth()->id().'_COLLECTIONS');
		if(Cache::tags($cacheKey)->has(':bookmarks')){
			$bookmarks = Cache::tags($cacheKey)->get(':bookmarks');
		}else{
			$bookmarks = UserGalleryCollections::where('user_id', auth()->id())->with(['gallery' => function($q){
				$q->select('id', 'title', 'j_title', 'slug')->where('status', 1);
			}])->where('type', 4)->select('gallery_id', 'type', 'created_at')->orderBy('created_at', 'DESC')->paginate(30);
			Cache::tags($cacheKey)->forever(':bookmarks', $bookmarks);
		}
		return view('user.profile.bookmark', compact('bookmarks'));
	}



}
