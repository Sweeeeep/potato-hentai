<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

use App\Jobs\ProcessUpload;
use App\Tag;
use App\User;
use App\Page;
use App\Gallery;

use App\TagContribution;
use App\GalleryContribution;

use Auth;
use Image;
use Cache;
use Session;
use Storage;
use PotatoHelper;
use Carbon\Carbon;
use PotatoFileHandler;

class ContributionController extends Controller{

	protected function saveFile(UploadedFile $file){
        $fileName = $this->createFilename($file);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        $dateFolder = date("Y-m-W");
        // Build the file path
		// $filePath = "upload/{$mime}/{$dateFolder}/";
        $filePath = "upload/temp/";
        $finalPath = storage_path("app/".$filePath);
        // move the file name
        $file->move($finalPath, $fileName);


        return response()->json([
            // 'path' => $filePath,
            'name' => $fileName
            // 'mime_type' => $mime
        ]);
    }

    protected function createFilename(UploadedFile $file){
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension
        // Add timestamp hash to name of the file
        $filename .= "_" . md5(time()) . "." . $extension;
        return $filename;
    }

    public function upload(){
		$category = Tag::search(2, '');
		$languages = Tag::search(3, '');

		return view('contributions.upload.index', compact('category', 'languages'));
	}

	public function uploadPost(Request $request){

		if(auth()->guest()){
			return response()->json([
				'status' => false,
				'message' => 'Please login first'
			]);
		}

		$data = $request->data;
		$user = User::getInfo(auth()->id());

		$fileInfo = PotatoFileHandler::getFileInfo($data['fileName']);

		$file = [
			'name' => $fileInfo['filename'],
			'extension' => '.'.$fileInfo['extension']
		];


		$gallery = new Gallery;
		$gallery->title = $data['title'];
		$gallery->j_title = $data['j_title'];
		$gallery->alt_title = $data['alt_title'];
		$gallery->description = $data['description'];
		$gallery->source = $data['source'];
		$gallery->status = 10;

		$user->gallery()->save($gallery);

		foreach ($data['tags'] as $group => $type) {
			foreach ($type as $key => $tag) {
				if($tag['id'] == 0){
					$title = str_replace('(new)', '', $tag['title']);
					$tag = Tag::firstOrCreate([
						'title' => trim($title),
						'type' => PotatoHelper::getTagGroupValue($group),
						'user_id' => $user->id,
						'status' => 10
					]);
					$gallery->tags()->attach($tag->id);
				}else{
					$gallery->tags()->attach($tag['id']);
				}
			}
		}

		$path = 'assets/images/gallery/'.$gallery->id;
		$tempPath = 'assets/images/gallery/'.$gallery->id.'/temp';
		$thumbPath = 'assets/images/gallery/'.$gallery->id.'/thumb';
		$fileNameExt = $file['name'].$file['extension'];

		Storage::disk('gallery')->makeDirectory($path);
		Storage::disk('gallery')->makeDirectory($tempPath);
		Storage::disk('gallery')->makeDirectory($thumbPath);

		$check = PotatoFileHandler::checkZip($path.'/'.$fileNameExt);
		if($check){
			$extract = PotatoFileHandler::extract($file, $tempPath);
			if($extract){
				$images = PotatoFileHandler::getImages($tempPath);
				$gallery->page()->save(new Page([
					'total_pages' => count($images)
				]));
				$cover = true;
				foreach ($images as $key => $image) {
					if($cover){
						$img = Image::make(public_path($image))->resize(227, 317);
						$img->encode('jpg');
						Storage::disk('gallery')->put($path.'/cover.jpg', $img);

						$cover = false;
					}
					$thumbImg = Image::make(public_path($image))->resize(140, 200);
					$thumbImg->encode('jpg', 50);
					Storage::disk('gallery')->put($thumbPath.'/'.($key+1).'_thumb.jpg', $thumbImg);

					$mainImg = Image::make(public_path($image));
					if($mainImg->filesize() >= 2097152){
						$mainImg->encode('jpg', 75);
					}else{
						$mainImg->encode('jpg');
					}
					Storage::disk('gallery')->put($path.'/'.($key+1).'.jpg', $mainImg);
				}
				Storage::disk('gallery')->deleteDirectory($tempPath);

				return 'asdas';
			}else{
				return 'unable to extract';
				// unable to extract the zip
			}
		}else{
			return 'unawnted files';
			// unwanted files
		}

	}

    public function fileUpload(Request $request) {
		if(auth()->guest()){
			return response()->json([
				'status' => false
			]);
		}
        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));
        // check if the upload is success
        if ($receiver->isUploaded()) {
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need
                return $this->saveFile($save->getFile());
            } else {
                // we are in chunk mode, lets send the current progress
                /** @var AbstractHandler $handler */
                $handler = $save->handler();
                return response()->json([
                    "done" => $handler->getPercentageDone(),
                ]);
            }
        } else {
            throw new UploadMissingFileException();
        }
    }


	public function tag(){
		return view('contributions.tag.index');
	}

	public function tagPost(Request $request){
		if(auth()->guest()){
			return response()->json([
				'status' => false,
				'message' => 'Please login first before to add new tag'
			]);
		}
		$data = $request->post;
		if(isset($data['title'], $data['category'])){
			$check = Tag::where('title', '=', $data['title'])->where('type', $data['category'])->first();
			if($check){
				return response()->json([
					'status' => false,
					'message' => 'already exists'
				]);
			}else{
				Tag::create([
					'user_id' => auth()->id(),
					'title' => $data['title'],
					'j_title' => $data['j_title'],
					'alt_title' => $data['alt_title'],
					'type' => $data['category'],
					'description' => $data['description'],
					'status' => 10,
				]);
				return response()->json([
					'status' => true,
					'message' => 'Successfully added new tag'
				]);
			}
		}
	}

	public function userContributions(){
		$cacheKey = 'POTATO_CONTRIBUTIONS';

		if(Cache::tags($cacheKey)->has(':galleries')){
			$galleries = Cache::tags($cacheKey)->get(':galleries');
		}else{
			$galleries = Gallery::where('status', 10)->with('user')->orderBy('created_at', 'DESC')->get();
			Cache::tags($cacheKey)->forever(':galleries', $galleries);
		}

		if(Cache::tags($cacheKey)->has(':tags')){
			$tags = Cache::tags($cacheKey)->get(':tags');
		}else{
			$tags = Tag::where('status', 10)->with('user')->orderBy('created_at', 'DESC')->get();
			Cache::tags($cacheKey)->forever(':tags', $tags);
		}

		if(Cache::tags($cacheKey)->has(':edit-galleries')){
			$editGalleries = Cache::tags($cacheKey)->get(':edit-galleries');
		}else{
			$editGalleries = GalleryContribution::where('status', '10')->with('user')->orderBy('created_at', 'DESC')->get();
			Cache::tags($cacheKey)->forever(':edit-galleries', $editGalleries);
		}

		if(Cache::tags($cacheKey)->has(':edit-tags')){
			$editTags = Cache::tags($cacheKey)->get(':edit-tags');
		}else{
			$editTags = TagContribution::where('status', '10')->with('user')->orderBy('created_at', 'DESC')->get();
			Cache::tags($cacheKey)->forever(':edit-tags', $editTags);
		}

		return view('contributions.index', compact('galleries', 'tags', 'editGalleries', 'editTags'));
	}

	public function viewGallery($slug){
		$gallery = Gallery::whereSlug($slug)->with(['tags' => function($q){
			$q->select('id', 'slug', 'title', 'type', 'status');
		}, 'page' => function($q){
			$q->select('total_pages', 'gallery_id');
		}, 'user' => function($q){
			$q->select('id', 'name');
		}])->firstOrFail();
		$sortedTags = PotatoHelper::sortTagsByGroup($gallery->tags);

		if(auth()->check()){
			$category = Tag::search(5, '');
			$languages = Tag::search(6, '');
		}else{
			$category = [];
			$languages = [];
		}

		return view('contributions.gallery.view', compact('gallery', 'sortedTags', 'category', 'languages'));
	}

	public function viewTag($slug){
		$tag = Tag::whereSlug($slug)->with('user')->orderBy('created_at', 'DESC')->firstOrFail();

		return view('contributions.tag.view', compact('tag'));
	}

	//user action methods

	public function updateGalleryContribute(Request $request){
		$gallery = Gallery::where('id', $request->id)->where('status', 10)->first();
		if($gallery){
			$gallery->title = $request->title;
			$gallery->j_title = $request->j_title;
			$gallery->alt_title = $request->alt_title;
			$gallery->description = $request->description;
			$tags = [
				$request->Category, $request->Language
			];
			foreach ($request->tags as $group => $type) {
				foreach ($type as $key => $tag) {
					if($tag['id'] == 0){
						$title = str_replace('(new)', '', $tag['title']);
						$tag = Tag::firstOrCreate([
							'title' => trim($title),
							'type' => PotatoHelper::getTagGroupValue($group),
							'user_id' => $gallery->user_id,
							'status' => 10
						]);
						$tags[] = $tag->id;
					}else{
						$tags[] = $tag['id'];
					}
				}
			}
			$gallery->tags()->sync($tags);
			$gallery->save();

			return response()->json([
				'status' => true,
				'message' => 'Successfully updated!'
			]);
		}else{
			return response()->json([
				'status' => false,
				'message' => 'Invalid GID'
			]);
		}
	}

	public function moderateGalleryUpload(Request $request){
		if(isset($request->id, $request->type)){
			$status = $request->type == 1 ? 1 : 0;
			$statusMsg = $request->type == 1 ? 'approved' : 'disapproved';

			$gallery = Gallery::where('id', $request->id);
			if($request->newTag){
				$gallery = $gallery->with(['tags' => function($q){
					$q->where('status', 10);
				}]);
			}
			$gallery = $gallery->first();
			$gallery->status = $status;
			// $gallery->approved_on = Carbon::now();
			$gallery->save();

			$msg = 'Successfully '.$statusMsg.' Gallery';

			if($request->newTag){
				foreach ($gallery->tags as $key => $tag) {
					if($tag->status == 10){
						$tag->status = $status;
						// $tag->approved_on = Carbon::now();
						$tag->save();
					}
				}
				$msg .= ' and '.$statusMsg.' new tags';
			}

			return response()->json([
				'status' => true,
				'message' => $msg
			]);
		}
	}

	public function moderateGalleryContribute(Request $request){

		if(isset($request->id, $request->type)){
			$check = GalleryContribution::where('id', $request->id)->where('status', 10)->with('tags')->first();
			if($check){
				$gallery = Gallery::where('id', $check->gallery_id)->with('tags')->first();
				if($gallery){
					$gallery->title = $check->title;
					$gallery->j_title = $check->j_title;
					$gallery->alt_title = $check->alt_title;
					$gallery->description = $check->description;
					$gallery->save();

					$tags = [];
					$oldTags = [];

					foreach ($check->tags as $key => $value) {
						if($request->newTag){
							if($value->status == 10){
								$value->status = 1;
								$value->save();
							}
						}
						$tags[] = $value->id;
					}

					foreach ($gallery->tags as $key => $value) {
						$oldTags[] = $value->id;
					}

					$check->tags()->sync($oldTags);
					$gallery->tags()->sync($tags);

					$check->status = 1;
					$check->save();

					return response()->json([
						'status' => true,
						'message' => 'Successfully'
					]);
				}
			}else{
				return response()->json([
					'status' => false,
					'message' => 'Invalid GalleryID'
				]);
			}
		}

	}


	public function moderateTagContribute(Request $request){
		if(isset($request->id, $request->type)){
			$tag = Tag::where('id', $request->id)->where('status', 10)->first();
			if($tag){
				$type = $request->type == 1 ? 'approved' : 'disapproved';
				$tag->status = $request->type;
				$tag->save();
				return response()->json([
					'status' => true,
					'message' => 'Successfully '.$type.' tag'
				]);
			}else{
				return response()->json([
					'status' => false,
					'message' => 'Invalid TagID'
				]);
			}
		}else{
			return response()->json([
				'status' => false,
				'message' => 'Invalid request'
			]);
		}
	}


}
