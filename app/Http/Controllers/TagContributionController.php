<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tag;
use App\TagContribution;

class TagContributionController extends Controller{


	public function requestTagEdit(Request $request){

		$check = TagContribution::where('tag_id', $request->id)->where('user_id', auth()->id())->where('status', 10)->first();
		if($check){
			return response()->json([
				'status' => false,
				'message' => 'You already request edit for this tag'
			]);
		}
		$tag = TagContribution::create([
			'user_id' => auth()->id(),
			'tag_id' => $request->id,
			'title' => $request->title,
			'j_title' => $request->j_title,
			'alt_title' => $request->alt_title,
			'description' => $request->description,
			'status' => 10
		]);

		return response()->json([
			'status' => true,
			'message' => 'You have successfully submitted your request, Thank you! '
		]);

	}

	public function tagRequestEditView($slug){
		$tag = TagContribution::whereSlug($slug)->with('tag', 'user')->firstOrFail();

		return view('contributions.edit-tag.view', compact('tag'));
	}

	public function updateRequestTagEdit(Request $request){

		$tag = Tag::where('id', $request->id)->where('status', 10)->first();

		if($tag){
			if($tag->user_id == auth()->id()){
				$tag->title = $request->title;
				$tag->j_title = $request->j_title;
				$tag->alt_title = $request->alt_title;
				$tag->description = $request->description;
				$tag->save();

				return response()->json([
					'status' => true,
					'message' => 'Successfully updated! reloading...'
				]);
			}else{
				return response()->json([
					'status' => false,
					'message' => 'You dont have permission to do this'
				]);
			}
		}
		return response()->json([
			'status' => false,
			'message' => 'Invalid ID'
		]);
	}

}
