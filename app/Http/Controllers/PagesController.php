<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class PagesController extends Controller{

	public function browse(){
		return view('browse');
	}

}
