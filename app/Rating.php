<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function gallery(){
		return $this->belongsTo(Gallery::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}
}
